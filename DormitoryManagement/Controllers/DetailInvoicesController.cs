﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dormitory.Business.Interface;
using Dormitory.Business.ViewModel;
using Dormitory.Entity.Data;
using Dormitory.Repositories.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace DormitoryManagement.Controllers
{
    public class DetailInvoicesController : Controller
    {
        private readonly DormitoryContext _context;
        IDetailInvoice _detailInvoice;
        IUnitOfWork _unitOfWork;
        IInvoice _invoice;

        public DetailInvoicesController(IDetailInvoice detailInvoice, DormitoryContext context, IUnitOfWork unitOfWork,
            IInvoice invoice)
            
        {
            _detailInvoice = detailInvoice;
            _context = context;
            _unitOfWork = unitOfWork;
            _invoice = invoice;
        }

        // GET: Diagrams
        public IActionResult Index()
        {

            return View();
        }

        #region Ajax API
        [HttpPost]
        public IActionResult SaveEntity(DetailInvoiceViewModel detailVm)
        {

            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                if (detailVm.KeyId == 0)
                {
                    var invoice = _invoice.GetLastest();
                    
                        detailVm.InvoiceFK = invoice;
                        _detailInvoice.Add(detailVm);
                    
                   // _detailInvoice.Add(detailVm);
                }
                else
                {
                    _detailInvoice.Update(detailVm);
                }
                _detailInvoice.Save();
                return new OkObjectResult(detailVm);
            }
        }
        #endregion


        //    [HttpGet]
        //    public IActionResult GetAll()
        //    {
        //        var model = _invoice.GetAll();
        //        return new OkObjectResult(model);
        //    }


        //    [HttpGet]
        //    public IActionResult GetAllPaging(int? status, string keyword, int page, int pageSize)
        //    {
        //        var model = _invoice.GetAllPaging(status, keyword, page, pageSize);
        //        return new OkObjectResult(model);
        //    }
        //    [HttpGet]
        //    public IActionResult GetById(int id)
        //    {
        //        var model = _invoice.GetById(id);
        //        return new OkObjectResult(model);
        //    }
        //    

        //    // GET: Diagrams/Delete/5
        //    public async Task<IActionResult> Delete(int? id)
        //    {
        //        if (id == null)
        //        {
        //            return NotFound();
        //        }

        //        var invoice = await _context.Invoice
        //            .Include(r => r.IdroomNavigation)
        //            .Include(r => r.IdemployeeNavigation)
        //            .FirstOrDefaultAsync(m => m.KeyId == id);
        //        if (invoice == null)
        //        {
        //            return NotFound();
        //        }

        //        return new JsonResult(invoice);
        //    }

        //    // POST: Diagrams/Delete/5
        //    [HttpPost, ActionName("Delete")]
        //    //[ValidateAntiForgeryToken]
        //    public async Task<IActionResult> DeleteConfirmed(int id)
        //    {
        //        var invoice = await _context.Invoice.FindAsync(id);
        //        _context.Invoice.Remove(invoice);
        //        await _context.SaveChangesAsync();
        //        return new JsonResult("Done");
        //    }

        //    private bool InvoiceExists(int id)
        //    {
        //        return _context.Invoice.Any(e => e.KeyId == id);
        //    }

    }
}