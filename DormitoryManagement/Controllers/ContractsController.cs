﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dormitory.Entity.Data;

namespace DormitoryManagement.Controllers
{
    public class ContractsController : Controller
    {
        private readonly DormitoryContext _context;

        public ContractsController(DormitoryContext context)
        {
            _context = context;
        }

        // GET: Contracts
        public async Task<IActionResult> Index()
        {
            var dormitoryContext = _context.Contract.Include(c => c.IdemployeeNavigation).Include(c => c.IdstudentNavigation);
            return View(await dormitoryContext.ToListAsync());
        }

        // GET: Contracts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contract = await _context.Contract
                .Include(c => c.IdemployeeNavigation)
                .Include(c => c.IdstudentNavigation)
                .FirstOrDefaultAsync(m => m.KeyId == id);
            if (contract == null)
            {
                return NotFound();
            }

            return View(contract);
        }

        // GET: Contracts/Create
        public IActionResult Create()
        {
            ViewData["Idemployee"] = new SelectList(_context.Employee, "KeyId", "KeyId");
            ViewData["Idstudent"] = new SelectList(_context.Student, "KeyId", "Email");
            return View();
        }

        // POST: Contracts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idstudent,DateStart,DateEnd,Idemployee,DateCreated,DateModified,KeyId")] Contract contract)
        {
            if (ModelState.IsValid)
            {
                _context.Add(contract);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idemployee"] = new SelectList(_context.Employee, "KeyId", "KeyId", contract.Idemployee);
            ViewData["Idstudent"] = new SelectList(_context.Student, "KeyId", "Email", contract.Idstudent);
            return View(contract);
        }

        // GET: Contracts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contract = await _context.Contract.FindAsync(id);
            if (contract == null)
            {
                return NotFound();
            }
            ViewData["Idemployee"] = new SelectList(_context.Employee, "KeyId", "KeyId", contract.Idemployee);
            ViewData["Idstudent"] = new SelectList(_context.Student, "KeyId", "Email", contract.Idstudent);
            return View(contract);
        }

        // POST: Contracts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idstudent,DateStart,DateEnd,Idemployee,DateCreated,DateModified,KeyId")] Contract contract)
        {
            if (id != contract.KeyId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(contract);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContractExists(contract.KeyId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idemployee"] = new SelectList(_context.Employee, "KeyId", "KeyId", contract.Idemployee);
            ViewData["Idstudent"] = new SelectList(_context.Student, "KeyId", "Email", contract.Idstudent);
            return View(contract);
        }

        // GET: Contracts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contract = await _context.Contract
                .Include(c => c.IdemployeeNavigation)
                .Include(c => c.IdstudentNavigation)
                .FirstOrDefaultAsync(m => m.KeyId == id);
            if (contract == null)
            {
                return NotFound();
            }

            return View(contract);
        }

        // POST: Contracts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var contract = await _context.Contract.FindAsync(id);
            _context.Contract.Remove(contract);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ContractExists(int id)
        {
            return _context.Contract.Any(e => e.KeyId == id);
        }
    }
}
