﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dormitory.Entity.Data;
using Dormitory.Business.ViewModel;
using Dormitory.Business.Interface;
using Dormitory.Repositories.Repositories;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace DormitoryManagement.Controllers
{
    public class FollowsController : Controller
    {
        private readonly DormitoryContext _context;
        private readonly IFollow _follow;
        private readonly IUnitOfWork _unitOfWork;

        public FollowsController(DormitoryContext context, IFollow follow , IUnitOfWork unitOfWork)
        {
            _context = context;
            _follow = follow;
            _unitOfWork = unitOfWork;
        }

        // GET: Follows
        public async Task<IActionResult> Index()
        {
            var dormitoryContext = _context.Follow.Include(f => f.IderrorNavigation).Include(f => f.IdstudentNavigation);
            return View(await dormitoryContext.ToListAsync());
        }
        #region AJAX API
        [HttpPost]
        public IActionResult SaveEntity(FollowViewModel followVm)
        {

            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                if (followVm.KeyId == 0)
                {

                    _follow.Add(followVm);
                }
                else
                {
                    _follow.Update(followVm);
                }
                _follow.Save();
                return new OkObjectResult(followVm);
            }
        }

      
        [HttpGet]
        public IActionResult GetAll()
        {
            var model = _follow.GetAll();
            return new OkObjectResult(model);
        }
      
        [HttpGet]
        public IActionResult GetAllPaging( string keyword, int page, int pageSize)
        {
            var model = _follow.GetAllPaging(keyword, page, pageSize);
            return new OkObjectResult(model);
        }
        [HttpGet]
        public IActionResult GetById(int id)
        {
            var model = _follow.GetById(id);
            return new OkObjectResult(model);
        }
        #endregion

        // GET: Follows/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var follow = await _context.Follow
                .Include(f => f.IderrorNavigation)
                .Include(f => f.IdstudentNavigation)
                .FirstOrDefaultAsync(m => m.KeyId == id);
            if (follow == null)
            {
                return NotFound();
            }

            return View(follow);
        }

        // GET: Follows/Create
        public IActionResult Create()
        {
            ViewData["Iderror"] = new SelectList(_context.Rules, "KeyId", "KeyId");
            ViewData["Idstudent"] = new SelectList(_context.Student, "KeyId", "Email");
            return View();
        }

        // POST: Follows/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idstudent,Iderror,Count,Date,DateCreated,DateModified,KeyId")] Follow follow)
        {
            if (ModelState.IsValid)
            {
                _context.Add(follow);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iderror"] = new SelectList(_context.Rules, "KeyId", "KeyId", follow.Iderror);
            ViewData["Idstudent"] = new SelectList(_context.Student, "KeyId", "Email", follow.Idstudent);
            return View(follow);
        }

        // GET: Follows/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var follow = await _context.Follow.FindAsync(id);
            if (follow == null)
            {
                return NotFound();
            }
            ViewData["Iderror"] = new SelectList(_context.Rules, "KeyId", "KeyId", follow.Iderror);
            ViewData["Idstudent"] = new SelectList(_context.Student, "KeyId", "Email", follow.Idstudent);
            return View(follow);
        }

        // POST: Follows/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Idstudent,Iderror,Count,Date,DateCreated,DateModified,KeyId")] Follow follow)
        {
            if (id != follow.KeyId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(follow);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FollowExists(follow.KeyId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Iderror"] = new SelectList(_context.Rules, "KeyId", "KeyId", follow.Iderror);
            ViewData["Idstudent"] = new SelectList(_context.Student, "KeyId", "Email", follow.Idstudent);
            return View(follow);
        }

        // GET: Follows/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var follow = await _context.Follow
                .Include(f => f.IderrorNavigation)
                .Include(f => f.IdstudentNavigation)
                .FirstOrDefaultAsync(m => m.KeyId == id);
            if (follow == null)
            {
                return NotFound();
            }

            return View(follow);
        }

        // POST: Follows/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var follow = await _context.Follow.FindAsync(id);
            _context.Follow.Remove(follow);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FollowExists(int id)
        {
            return _context.Follow.Any(e => e.KeyId == id);
        }
    }
}
