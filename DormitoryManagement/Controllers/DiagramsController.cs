﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dormitory.Entity.Data;
using Dormitory.Business.Interface;
using Dormitory.Repositories.Repositories;
using Dormitory.Business.ViewModel;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace DormitoryManagement.Controllers
{
    public class DiagramsController : Controller
    {
        private readonly DormitoryContext _context;
        IDiagram _diagram;
        ICategoryRoom _categoryRoom;
        IUnitOfWork _unitOfWork;

        public DiagramsController(IDiagram diagram, DormitoryContext context, IUnitOfWork unitOfWork, ICategoryRoom categoryRoom)
        {
            _diagram = diagram;
            _context = context;
            _unitOfWork = unitOfWork;
            _categoryRoom = categoryRoom;
        }

        // GET: Diagrams
        public async Task<IActionResult> Index()
        {

            ViewData["Idcategory"] = new SelectList(_context.CategoryRoom, "KeyId", "Description");
            ViewData["Idemployee"] = new SelectList(_context.Employee, "KeyId", "Name");
            ViewData["Idzone"] = new SelectList(_context.Zone, "KeyId", "Name");
            return View();
        }

        #region Ajax API
        [HttpPost]
        public IActionResult SaveEntity(RoomViewModel roomVm)
        {
            
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                if (roomVm.KeyId == 0)
                {

                    _diagram.Add(roomVm);
                }
                else
                {
                    _diagram.Update(roomVm);
                }
                _diagram.Save();
                return new OkObjectResult(roomVm);
            }
        }

        [HttpPost]
        public IActionResult Reversation(int id)
        {
            if (id == 0)
            {
                return new BadRequestResult();
            }
            else
            {
                var query = _diagram.GetById(id);
                if (query.Reservation == 0)
                    return new BadRequestResult();
                else
                {
                    _diagram.Reversation(id);
                    return new OkObjectResult(id);

                }
                
            }

        }
        [HttpGet]
        public IActionResult GetAll()
        {
            var model = _diagram.GetAll();
            return new OkObjectResult(model);
        }
        [HttpGet]
        public IActionResult GetAllCategory()
        {
            var model = _categoryRoom.GetAll();
            return new OkObjectResult(model);
        }
        [HttpGet]
        public IActionResult GetCategoryById(int id)
        {
            var model = _diagram.GetById(id);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAllPaging(int? categoryID,int? status,string keyword,int page ,int pageSize)
        {
            var model = _diagram.GetAllPaging(categoryID,status,keyword,page,pageSize);
            return new OkObjectResult(model);
        }
        [HttpGet]
        public IActionResult GetById(int id)
        {
            var model = _diagram.GetById(id);
            return new OkObjectResult(model);
        }
        #endregion

        // GET: Diagrams/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var room = await _context.Room
                .Include(r => r.IdcategoryNavigation)
                .Include(r => r.IdemployeeNavigation)
                .Include(r => r.IdzoneNavigation)
                .FirstOrDefaultAsync(m => m.KeyId == id);
            if (room == null)
            {
                return NotFound();
            }

            return new JsonResult(room);
        }

        // POST: Diagrams/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var room = await _context.Room.FindAsync(id);
            _context.Room.Remove(room);
            await _context.SaveChangesAsync();
            return new JsonResult("Done");
        }

        private bool RoomExists(int id)
        {
            return _context.Room.Any(e => e.KeyId == id);
        }
    }    
}
