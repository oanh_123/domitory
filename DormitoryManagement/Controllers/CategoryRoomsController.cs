﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dormitory.Entity.Data;
using Dormitory.Repositories.Repositories;
using Dormitory.Business.Interface;

namespace DormitoryManagement.Controllers
{
    public class CategoryRoomsController : Controller
    {
        private readonly DormitoryContext _context;
        ICategoryRoom _categoryRoom;
        IUnitOfWork _unitOfWork;

        public CategoryRoomsController(ICategoryRoom categoryRoom, DormitoryContext context, IUnitOfWork unitOfWork)
        {
            _categoryRoom = categoryRoom;
            _context = context;
            _unitOfWork = unitOfWork;
        }

        public CategoryRoomsController(DormitoryContext context)
        {
            _context = context;
        }
        #region AJAX API
        [HttpGet]
        public IActionResult GetAll()
        {
            var model = _categoryRoom.GetAll();
            return new OkObjectResult(model);
        }
        #endregion
        // GET: CategoryRooms
        public async Task<IActionResult> Index()
        {
            return View(await _context.CategoryRoom.ToListAsync());
        }

        // GET: CategoryRooms/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoryRoom = await _context.CategoryRoom
                .FirstOrDefaultAsync(m => m.KeyId == id);
            if (categoryRoom == null)
            {
                return NotFound();
            }

            return View(categoryRoom);
        }

        // GET: CategoryRooms/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CategoryRooms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Description,UnitPrice,Amount,DateCreated,DateModified,KeyId")] CategoryRoom categoryRoom)
        {
            if (ModelState.IsValid)
            {
                _context.Add(categoryRoom);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(categoryRoom);
        }

        // GET: CategoryRooms/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoryRoom = await _context.CategoryRoom.FindAsync(id);
            if (categoryRoom == null)
            {
                return NotFound();
            }
            return View(categoryRoom);
        }

        // POST: CategoryRooms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Description,UnitPrice,Amount,DateCreated,DateModified,KeyId")] CategoryRoom categoryRoom)
        {
            if (id != categoryRoom.KeyId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(categoryRoom);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CategoryRoomExists(categoryRoom.KeyId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(categoryRoom);
        }

        // GET: CategoryRooms/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoryRoom = await _context.CategoryRoom
                .FirstOrDefaultAsync(m => m.KeyId == id);
            if (categoryRoom == null)
            {
                return NotFound();
            }

            return View(categoryRoom);
        }

        // POST: CategoryRooms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var categoryRoom = await _context.CategoryRoom.FindAsync(id);
            _context.CategoryRoom.Remove(categoryRoom);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CategoryRoomExists(int id)
        {
            return _context.CategoryRoom.Any(e => e.KeyId == id);
        }
    }
}
