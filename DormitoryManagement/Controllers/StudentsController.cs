﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dormitory.Entity.Data;
using DormitoryManagement.Helper;
using System.IO;
using Dormitory.Business.Interface;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;

namespace DormitoryManagement.Controllers
{
    public class StudentsController : Controller
    {
        private readonly DormitoryContext _context;
        private IDiagramRepository _diagramRepository;
        private IUnitOfWork _unitOfWork;

        public StudentsController(DormitoryContext context, IDiagramRepository diagramRepository, IUnitOfWork unitOfWork)
        {
            _context = context;
            _diagramRepository = diagramRepository;
            _unitOfWork = unitOfWork;
        }

        // GET: Students
        public async Task<IActionResult> Index()
        {
            var dormitoryContext = _context.Student.Include(s => s.IdroomNavigation);
            return View(await dormitoryContext.ToListAsync());
        }

        // GET: Students/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Student              
                .Include(s => s.IdroomNavigation)
                .FirstOrDefaultAsync(m => m.KeyId == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // GET: Students/Create
        public  IActionResult Create()
        {
            var student = new Student();                       
            ViewData["Idroom"] = new SelectList(_context.Room, "KeyId", "Name");
            return PartialView("CreatePartialModal", student);
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("KeyId,IdStudent,Name,Gender,DOB,Faculty,Class,PhoneNumber,Address,PhoneParents,AddressOfParents,PolicyArea,Teacher,Idroom,Email,Idcard,Image")] Student student, Microsoft.AspNetCore.Http.IFormFile image)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    //Set Key Name
                    string ImageName = Guid.NewGuid().ToString() + Path.GetExtension(image.FileName);

                    //Get url To Save
                    string SavePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images/avatar/", ImageName);

                    using (var stream = new FileStream(SavePath, FileMode.Create))
                    {
                        image.CopyTo(stream);
                    }
                    student.Image = ImageName;
                }
                var query = _diagramRepository.FindById(student.Idroom);
                query.Reservation -= 1;
                _unitOfWork.Commit();
                _context.Add(student);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }         
            ViewData["Idroom"] = new SelectList(_context.Room, "KeyId", "Name", student.Idroom);
            return RedirectToAction(nameof(Index));
        }     

        // GET: Students/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Student
                .Include(s => s.IdroomNavigation.IdcategoryNavigation)
                .FirstOrDefaultAsync(x => x.KeyId == id);
            if (student == null)
            {
                return NotFound();
            }
            ViewData["Idroom"] = new SelectList(_context.Room, "KeyId", "Name", student.Idroom);
            return View(student);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("KeyId,IdStudent,Name,Gender,DOB,Faculty,Class,PhoneNumber,Address,PhoneParents,AddressOfParents,PolicyArea,Teacher,Idroom,Email,Idcard,Image")] Student student , Microsoft.AspNetCore.Http.IFormFile image)
        {
            if (id != student.KeyId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    if (image != null)
                    {
                        //Set Key Name
                        string ImageName = Guid.NewGuid().ToString() + Path.GetExtension(image.FileName);

                        //Get url To Save
                        string SavePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images/avatar/", ImageName);

                        using (var stream = new FileStream(SavePath, FileMode.Create))
                        {
                            image.CopyTo(stream);
                        }
                        student.Image = ImageName;
                    }
                    _context.Update(student);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentExists(student.KeyId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Edit");
            }
            ViewData["Idroom"] = new SelectList(_context.Room, "KeyId", "Idroom", student.Idroom);
            return RedirectToAction("Edit");
        }

        // GET: Students/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Student
                .Include(s => s.IdroomNavigation)
                .Include(s=> s.Contract)
                .Include(s=> s.Follow)
                .FirstOrDefaultAsync(m => m.KeyId == id);
            if (student == null)
            {
                return NotFound();
            }

            return new JsonResult(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var student = await _context.Student.FindAsync(id);
            _context.Student.Remove(student);
            await _context.SaveChangesAsync();
            return new JsonResult("Done");
        }

        private bool StudentExists(int id)
        {
            return _context.Student.Any(e => e.KeyId == id);
        }
        /*------------------------------------------------------------------------------------------------------*/
        public async Task<IActionResult> LoadDataWithPaging(Helper.DTParameters dtParameters)
        {
            var searchBy = dtParameters.Search?.Value;

            var orderCriteria = string.Empty;
            var orderAscendingDirection = true;

            if (dtParameters.Order != null)
            {
                // in this example we just default sort on the 1st column
                orderCriteria = dtParameters.Columns[dtParameters.Order[0].Column].Data;
                orderAscendingDirection = dtParameters.Order[0].Dir.ToString().ToLower() == "asc";
            }
            else
            {
                // if we have an empty search then just order the results by Id ascending
                orderCriteria = "KeyId";
                orderAscendingDirection = true;
            }

            var result = await _context.Student
                .ToListAsync();

            //var rooms = new List<Room>();
            //try
            //{
            //    foreach (var item in result)
            //    {
            //        var room = _diagramRepository.FindById(item.Idroom);

            //        rooms.Add(room);

            //    }
            //}
            //catch (Exception e)
            //{

            //    throw;
            //}

            var totalResultsCount = result.Count();
            if (!string.IsNullOrEmpty(searchBy))
            {
                result = result.Where(r => r.Name.ToLower().Contains(searchBy.ToLower()) || r.KeyId.Equals(searchBy) || r.IdroomNavigation.Name.Equals(searchBy)).ToList();
            }
            result = orderAscendingDirection ?
                result.AsQueryable().OrderByDynamic(orderCriteria, LinqExtension.Order.Asc).ToList() :
                result.AsQueryable().OrderByDynamic(orderCriteria, LinqExtension.Order.Desc).ToList();

            var filteredResultsCount = result.Count();
            return new JsonResult(new
            {
                draw = dtParameters.Draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result.Skip(dtParameters.Start).Take(dtParameters.Length),
                //room = rooms
            });


        }
    }
}
