﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dormitory.Entity.Data;
using Dormitory.Business.Interface;
using Dormitory.Repositories.Repositories;
using Dormitory.Business.ViewModel;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using AutoMapper;

namespace DormitoryManagement.Controllers
{
    public class InvoicesController : Controller
    {
        private readonly DormitoryContext _context;
        IInvoice _invoice;
        IUnitOfWork _unitOfWork;
        IService _service;
        IDetailInvoice _detailInvoice;

        public InvoicesController(IInvoice invoice, DormitoryContext context, IUnitOfWork unitOfWork , 
            IService service, IDetailInvoice detailInvoice)
        {
            _invoice = invoice;
            _context = context;
            _unitOfWork = unitOfWork;
            _service = service;
            _detailInvoice = detailInvoice;
        }

        // GET: Diagrams
        public IActionResult Index()
        {
            ViewData["Idroom"] = new SelectList(_context.Room, "KeyId", "Name");
            ViewData["Idemployee"] = new SelectList(_context.Employee, "KeyId", "Name");
            return View();
        }

        #region Ajax API
        [HttpPost]
        public IActionResult SaveEntity(InvoiceViewModel invoiceVm)
        {

            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                if (invoiceVm.KeyId == 0)
                {
                    _invoice.Add(invoiceVm);
                }
                else
                {
                    _invoice.Update(invoiceVm);
                }
                _invoice.Save();
               
                return new OkObjectResult(invoiceVm);
            }
        }

       
        [HttpGet]
        public IActionResult GetAll()
        {
            var model = _invoice.GetAll();
            return new OkObjectResult(model);
        }
        [HttpGet]
        public IActionResult GetService()
        {
            var model = _service.GetAll();
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAllPaging( int? status, string keyword, int page, int pageSize)
        {
            var model = _invoice.GetAllPaging(status, keyword, page, pageSize);
            return new OkObjectResult(model);
        }
        [HttpGet]
        public IActionResult GetById(int id)
        {
            var model = _invoice.GetById(id);
            return new OkObjectResult(model);
        }
        #endregion

        // GET: Diagrams/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoice = await _context.Invoice
                .Include(r => r.IdroomNavigation)
                .Include(r => r.IdemployeeNavigation)
                .FirstOrDefaultAsync(m => m.KeyId == id);
            if (invoice == null)
            {
                return NotFound();
            }

            return new JsonResult(invoice);
        }

        // POST: Diagrams/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var invoice = await _context.Invoice.FindAsync(id);
            _context.Invoice.Remove(invoice);
            await _context.SaveChangesAsync();
            return new JsonResult("Done");
        }

        private bool InvoiceExists(int id)
        {
            return _context.Invoice.Any(e => e.KeyId == id);
        }
    }
}
