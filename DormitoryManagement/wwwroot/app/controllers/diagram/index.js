﻿var diagramController = function () {
    this.initialize = function () {
        loadCategories();
        loadData();
        registerEvents();
    }
  

    function registerEvents() {
        $('#ddlShowPage').on('change', function () {
            dormitory.configs.pageSize = $(this).val();
            dormitory.configs.pageIndex = 1;
            loadData(true);
        });
        $('#txtKeyword').on('keyup', function (e) {
            if (e.keyCode === 13) {
                loadData();
            }
        });
        $('#Search').on('click', function (e) {
            loadData();
        });
        $('body').on('click', '.reversation', function (e) {
           // if (confirm("Bạn muốn đặt chỗ ?")) {
                var that = $(this).attr('data-id');
                $.ajax({
                    type: 'POST',
                    url: '/Diagrams/Reversation',
                    data: { id: that },
                    dataType: 'json',
                    beforeSend: function () {
                        dormitory.startLoading();
                    },
                    success: function (response) {
                        console.log("data", response)
                        dormitory.notify('Đặt chỗ thành công !', 'success');
                        loadData();
                        dormitory.stopLoading();

                    },
                    error: function (status) {
                        dormitory.notify('Đã hết chỗ để đặt !', 'error');
                        dormitory.stopLoading();
                    },
                });
            //}
            //else {
            //    alert("Bạn vừa bấm hủy");
            //}
        })
       
        var placeholderElement = $('#modal-placeholder-diagram');
        var url = placeholderElement.data('url');
        $.get(url).done(function (data) {
            placeholderElement.html(data);
        });
        $('#createRoom').click(function () {
            $('#Idzone').val('');
            $('#Idcategory').val('');
            $('#Idemployee').val('');
            $('#Name').val('');
            $('#Status').val('');
            $('#modal-add-edit').modal('show');
        }); 
        //$('#card-content').click(function () {
           
        //    $('#modal-detail').modal('show');
        //});  
        $('body').on('click', '.editRoom', function (e) {
            e.preventDefault();
            var that = $(this).attr('data-id');
            $.ajax({
                type: "GET",
                url: "/Diagrams/GetById",
                data: { id: that },
                dataType: "json",
                beforeSend: function () {                   
                    dormitory.startLoading();
                },
                success: function (response) {
                    var data = response;
                    $('#addContactLabel').html("Edit Room");
                    $('#txtkeyId').val(data.keyId);
                    $('#txtName').val(data.name);
                    $('#txtZone').val(data.idzone);
                    $('#txtCategory').val(data.idcategory);
                    $('#txtEmployee').val(data.idemployee);
                    $('#Status').val(data.status);
                    $('#modal-add-edit').modal('show');
                    dormitory.stopLoading();
                },
                error: function (status) {
                    dormitory.notify('Có lỗi xảy ra', 'error');
                    dormitory.stopLoading();
                }
            });

        });
        $('body').on('click', '.detailRoom', function (e) {
            e.preventDefault();
            var that = $(this).attr('data-id');
            var template = $('#table-template').html();
            var render = "";
            $.ajax({
                type: "GET",
                url: "/Diagrams/GetById",
                data: { id: that },
                dataType: "json",
                beforeSend: function () {
                    dormitory.startLoading();
                },
                success: function (response) {
                    console.log("data", response);
                    $.each(response.student, function (i, item) {
                        render += Mustache.render(template, {
                            ID: item.idStudent,
                            Name: item.name,
                            Class: item.class,
                            DOB: moment(item.dob).format("DD/MM/YYYY"),
                            Faculty: item.faculty,
                            Email: item.email
                        });                        
                    });
                    $('#tbl-content').html(render);
                    $('#modal-detail').modal('show');
                    
                },
                error: function (status) {
                    dormitory.notify('Có lỗi xảy ra', 'error');
                    dormitory.stopLoading();
                }
            });

        });



        $('#btnSave').click(function (e) {
            if ($('#formRoom').valid()) {
                e.preventDefault();
                var keyId;
                if ($('#txtkeyId').val() === "") {
                    keyId = 0;
                }
                else {
                    keyId = parseInt($('#txtkeyId').val());
                }
                var statusFK;
                var idcategory = $('#txtCategory').val();    
                var idzone = $('#txtZone').val();
                var idemployee = $('#txtEmployee').val();
                var name = $('#txtName').val();
                $.ajax({
                    type: 'POST',
                    url: '/Diagrams/SaveEntity',
                    data: {
                        KeyId: keyId,
                        Idcategory: idcategory,
                        Idzone: idzone,
                        Idemployee: idemployee,
                        Status : 0,
                        Name: name,
                        Reservation : 0,
                    },
                    dataType: "json",
                    beforeSend: function () {
                        dormitory.startLoading();
                    },
                    success: function (response) {
                        console.log(response);
                        $('#modal-add-edit').modal('hide');
                        dormitory.notify('Success !', 'success');
                        $('#formRoom').trigger('reset');
                        dormitory.stopLoading();
                        loadData();
                    },
                    error: function (err) {
                        dormitory.notify('Cannot load data !', 'error');
                        dormitory.stopLoading();

                    },
                });
                return false;
            }
        });


        // Delete
        $('body').on('click', '.delRoom', function (e) {
            var id = $(this).attr("data-id");
            const swalWithBootstrapButtons = Swal.mixin({
                buttonsStyling: true
            });

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                confirmButtonColor: "#5cb85c",
                cancelButtonText: 'No, cancel!',
                cancelButtonColor: "#d9534f",
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: "POST",
                        url: "/Diagrams/Delete",
                        data: { id: id },
                        //headers: { "RequestVerificationToken": $('#hiddenForm input[name="__RequestVerificationToken"]').val() },
                        success: function () {
                            //table.row(tableRow).remove().draw();
                            swalWithBootstrapButtons.fire(
                                'Deleted!',
                                'Record has been deleted!',
                                'success'
                            );
                            loadData(true);
                        },
                        error: function (data, status, error) {
                            alert('error');
                            console.log(status);
                        }
                    });
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Nothing changed!',
                        'error'
                    );
                }
            });
        });
    }

    function loadData(isPageChanged) {
       
        var template = $('#card-room').html();
        var render = "";
        var render2 = "";
        $.ajax({
            type: 'GET',
            data: {
                categoryID: $('#ddlCategorySearch').val(),
                status: $('#ddlStatus').val(),
                keyword: $('#txtKeyword').val(),
                page: dormitory.configs.pageIndex,
                pageSize: dormitory.configs.pageSize
            },
            url: '/Diagrams/GetAllPaging',
            dataType: 'json',
            beforeSend: function () {
                dormitory.startLoading();
            },
            success: function (response) {
               
                var sumBeds = 0;
                var sumBeds2 = 0;
                var sumEmptyBeds2 = 0;
                var sumStudent2 = 0;
                var sumEmptyBeds = 0;
                var sumStudent = 0;
                var sumReservation = 0;
                var sumReservation2 = 0;
                var status = "<option value=''>--Choose Status--</option>";
                $.each(response.results, function (i, item) {
                    if (item.idzone === 1) {
                        render += Mustache.render(template, {
                            KeyId: item.keyId,
                            Name: item.name,
                            TotalBed: item.idcategoryNavigation.amount,
                            Student: item.student.length,
                            EmptyBed: item.idcategoryNavigation.amount - item.student.length,
                            Reservation: item.reservation,                          
                        });                     
                        sumBeds += item.idcategoryNavigation.amount;
                        sumEmptyBeds += item.idcategoryNavigation.amount - item.student.length;
                        sumStudent += item.student.length;
                        sumReservation += item.reservation;
                    }
                    else {
                        render2 += Mustache.render(template, {
                            KeyId: item.keyId,
                            Name: item.name,
                            TotalBed: item.idcategoryNavigation.amount,
                            Student: item.student.length,
                            EmptyBed: item.idcategoryNavigation.amount - item.student.length,
                            Reservation: item.reservation,
                        });
                        sumBeds2 += item.idcategoryNavigation.amount;
                        sumEmptyBeds2 += item.idcategoryNavigation.amount - item.student.length;
                        sumStudent2 += item.student.length;
                        sumReservation2 += item.reservation;
                    }
                                                  
                });
                status = '<option value="">Status</option>' + '<option value="1">Full</option>'+"<option value='0'>Still Empty</option>"+'<option value="2">Can Order</option>';
                $('#TotalRoom').text(response.rowCount);
                $('#SumBed').text(sumBeds);
                $('#SumEmptyBed').text(sumEmptyBeds);
                $('#SumStudent').text(sumStudent);
                $('#SumReservation').text(sumReservation);
                $('#lblTotalRecords').text(response.rowCount);
                $('#card-content').html(render);
                $('#card-content-b').html(render2);
                $('#SumBed2').text(sumBeds2);
                $('#SumEmptyBed2').text(sumEmptyBeds2);
                $('#SumStudent2').text(sumStudent2);
                $('#SumReservation2').text(sumReservation2);
                $('#ddlStatus').html(status);
                var hi = $('#canOrder').val();
              

                dormitory.stopLoading();
                wrapPaging(response.rowCount, function () {
                    loadData();
                }, isPageChanged);

            },
            error: function (status) {
                console.log(status);
                dormitory.notify('Cannot loading data', 'error');

            }
        });

    }
    function wrapPaging(recordCount, callBack, changePageSize) {
        var totalsize = Math.ceil(recordCount / dormitory.configs.pageSize);
        //Unbind pagination if it existed or click change pagesize
        if ($('#paginationUL a').length === 0 || changePageSize === true) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
        //Bind Pagination Event
        if (totalsize > 0)
            $('#paginationUL').twbsPagination({
                totalPages: totalsize,
                visiblePages: 7,
                first: 'First',
                prev: 'Previous',
                next: 'Next',
                last: 'Last',
                onPageClick: function (event, p) {
                    dormitory.configs.pageIndex = p;
                    setTimeout(callBack(), 200);
                }
            });
    }

    function loadCategories(){
        $.ajax({
            type: 'GET',
            data: {},
            url: '/Diagrams/GetAllCategory',
            dataType: 'json',
            beforeSend: function () {
                dormitory.startLoading();
            },
            success: function (response) {
                var render = "<option value=''>--Choose Category--</option>";

                $.each(response, function (i, item) {
                    render += "<option id='selectCate' value='" + item.keyId + "'>" + item.description + "</option>";                   
                });
                $('#ddlCategorySearch').html(render);
            },
        error: function (status) {
            console.log(status);
            dormitory.notify('Cannot loading category data', 'error');
                }
            });
    }
   
  
}
