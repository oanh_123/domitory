﻿var invoiceController = function () {
    var cacheObj = {
        service: []
    };
    this.initialize = function () {
        $.when(loadService()).done(function () {
            loadData();
        });

        registerEvents();
    };
    function registerEvents() {
        $('#btnCreate').click(function () {
            resetForm();
            $('#modal-add-edit').modal('show');
        });
        $('body').on('click', '.btn-edit',function(e){
            e.preventDefault();
            var that = $(this).attr('data-id');
            var template = $('#InvoiceDetail-table-template').html();
            var render = '';
            $.ajax({
                type: "GET",
                url: "/Invoices/GetById",
                data: { id: that },
                dataType: "json",
                beforeSend: function () {
                    dormitory.startLoading();
                },
                success: function (response) {
                    console.log("editInvoice", response);
                    var data = response;
                    $('#txtkeyId').val(data.keyId);
                    $('#txtName').val(data.name);
                    $('#txtRoom').val(data.idroom);
                    $('#txtEmployee').val(data.idemployee);
                    $('#addContactLabel').html("Edit Invoice");
                    $.each(data.detailInvoice, function (i, item) {
                        render += Mustache.render(template, {
                            KeyId: item.keyId,
                            NameService: item.serviceFKNavigation.nameService,
                            Quantity: item.number
                        });
                    });
                    $('#InvoiceDetail-tbl-content').html(render);
                    $('#modal-add-edit').modal('show');
                    dormitory.stopLoading();
                },
                error: function (status) {
                    dormitory.notify('Có lỗi xảy ra', 'error');
                    dormitory.stopLoading();
                }
            });

        });
        $('body').on('click', '.btn-detail', function (e) {
            e.preventDefault();
            var that = $(this).attr('data-id');
            var render = '';
            var template = $('#table-detail').html();
            $.ajax({
                type: "GET",
                url: "/Invoices/GetById",
                data: { id: that },
                dataType: "json",
                beforeSend: function () {
                    dormitory.startLoading();
                },
                success: function (response) {
                    var data = response;
                    $('#InvoiceName').text(data.name);
                    $('#RoomName').text(data.idroomNavigation.name);
                    $('#DateInvoice').text(moment(data.date).format("DD/MM/YYYY"));
                    var stt = 1;
                    var sub = 0;
                    var sum = 0; 
                    var vat = 0;
                    $.each(data.detailInvoice, function (i, item) {
                        render += Mustache.render(template, {
                            Stt: stt,
                            Name: item.serviceFKNavigation.nameService,
                            Number: item.number,
                            UnitCost: dormitory.formatNumber(item.serviceFKNavigation.unitPrice),
                            Total: dormitory.formatNumber(item.money),
                            
                        });
                        sub += item.money;
                        vat = sub * 0.1;
                        sum = sub + vat;
                        stt++;                    
                    });                 
                    $('#tbl-detail').html(render);                                      
                    $('#Room').text(data.idroom);
                    $('#SubMoney').text(dormitory.formatNumber(sub));
                    $('#VAT').text(dormitory.formatNumber(vat));
                    $('#SumMoney').text(dormitory.formatNumber(sum));
                    $('#modal-edit').modal('show');
                    dormitory.stopLoading();
                },
                error: function (status) {
                    dormitory.notify('Có lỗi xảy ra', 'error');
                    dormitory.stopLoading();
                }
            });

        });
        $('#btnSave').click(function (e) {
            if ($('#formInvoice').valid()) {        
                e.preventDefault();
                var keyId;
                if ($('#txtkeyId').val() === "") {
                    keyId = 0;
                }
                else {
                    keyId = parseInt($('#txtkeyId').val());
                }
                console.log(keyId);
                var idroom = $('#txtRoom').val();
                var idemployee = $('#txtEmployee').val();
                var name = $('#txtName').val();
                // Detail Invoice
                var detailInvoice = [];
                $.each($('#InvoiceDetail-tbl-content tr'), function (i, item) {
                    detailInvoice.push({
                        //KeyId: $('.ddlService option:selected').val(),                    
                        KeyId: $(item).data('id'),
                        ServiceFK: $(item).find('select.ddlService').first().val(),
                        Number: $(item).find('input.txtQuantity').first().val(),
                        InvoiceFK : keyId
                    })
                })
                $.ajax({
                    type: 'POST',
                    url: '/Invoices/SaveEntity',
                    data: {
                        KeyId : keyId,
                        Idroom: idroom,
                        Idemployee: idemployee,
                        Name: name,
                        Status: 0,
                        DetailInvoice: detailInvoice,
                    },
                    dataType: "json",
                    beforeSend: function () {
                        dormitory.startLoading();
                    },
                    success: function (response) {                      
                        console.log(response);
                        $('#modal-add-edit').modal('hide');
                        dormitory.notify('Success !', 'success');
                        $('#formInvoice').trigger('reset');
                        dormitory.stopLoading();
                        loadData();
                    },
                    error: function (err) {
                        dormitory.notify('Cannot load data !', 'error');
                        dormitory.stopLoading();

                    },
                });
                return false;
            }
        });
      
        $('#btnAddDetail').on('click', function () {
            var template = $('#InvoiceDetail-table-template').html();
            var services = getServiceOptions(null);
            var render = Mustache.render(template,
                {
                    KeyId : 0 ,
                    Quantity: 0,
                    NameService: services
                });
            
            $('#InvoiceDetail-tbl-content').append(render);

        });
        $('body').on('click', '.btn-delete-detail', function () {
            $(this).parent().parent().remove();
        });

        $('body').on('click', '.btn-delete', function (e) {
            var id = $(this).attr("data-id");
            const swalWithBootstrapButtons = Swal.mixin({
                buttonsStyling: true
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                confirmButtonColor: "#5cb85c",
                cancelButtonText: 'No, cancel!',
                cancelButtonColor: "#d9534f",
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: "POST",
                        url: "/Invoices/Delete",
                        data: { id: id },
                        //headers: { "RequestVerificationToken": $('#hiddenForm input[name="__RequestVerificationToken"]').val() },
                        success: function () {
                            //table.row(tableRow).remove().draw();
                            swalWithBootstrapButtons.fire(
                                'Deleted!',
                                'Record has been deleted!',
                                'success'
                            );
                            loadData(true);
                        },
                        error: function (data, status, error) {
                            alert('error');
                            console.log(status);
                        }
                    });
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Nothing changed!',
                        'error'
                    );
                }
            });
        });
    };
    function resetForm() {
        $('#txtName').val('');
        $('#txtRoom').val('');
        $('#txtEmployee').val('');
        $('#InvoiceDetail-tbl-content').val('');
    }
    function loadData(isPageChanged) {
        var template = $('#table-template').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {

                status: $('#ddlStatus').val(),
                keyword: $('#txtKeyword').val(),
                page: dormitory.config.pageIndex,
                pageSize: dormitory.config.pageSize
            },
            url: '/Invoices/GetAllPaging',
            dataType: 'json',
            beforeSend: function () {
                dormitory.startLoading();
            },
            success: function (response) {
                            
                var status = "<option value=''>--Choose Status--</option>";
                $.each(response.results, function (i, item) {
                    render += Mustache.render(template, {
                        KeyId: item.keyId,
                        Date: moment(item.date).format("DD/MM/YYYY"),
                        Name: item.name,
                        Room: item.idroomNavigation.name,
                        Employee: item.idemployeeNavigation.name,
                        TotalMoney: dormitory.formatNumber(item.totalMoney),
                        Status: dormitory.getStatus(item.status),
                    });
                }),
                   
                status = '<option value="">Status</option>' + '<option value="1">Full</option>' + "<option value='0'>StillEmpty</option>";              
                $('#lblTotalRecords').text(response.rowCount);
                $('#tbl-content').html(render);              
                $('#ddlStatus').html(status);

                dormitory.stopLoading();
                wrapPaging(response.rowCount, function () {
                    loadData();
                }, isPageChanged);

            },
            error: function (status) {
                console.log(status);
                dormitory.notify('Cannot loading data', 'error');

            }
        });

    }
    function wrapPaging(recordCount, callBack, changePageSize) {
        var totalsize = Math.ceil(recordCount / dormitory.config.pageSize);
        //Unbind pagination if it existed or click change pagesize
        if ($('#paginationUL a').length === 0 || changePageSize === true) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
        //Bind Pagination Event
        if (totalsize > 0)
            $('#paginationUL').twbsPagination({
                totalPages: totalsize,
                visiblePages: 7,
                first: 'First',
                prev: 'Previous',
                next: 'Next',
                last: 'Last',
                onPageClick: function (event, p) {
                    dormitory.config.pageIndex = p;
                    setTimeout(callBack(), 200);
                }
            });
    }
    function loadService() {
        return $.ajax({
            type: 'GET',
            data: {},
            url: '/Invoices/GetService',
            dataType: 'json',
            beforeSend: function () {
                dormitory.startLoading();
            },
            success: function (response) {
                cacheObj.service = response;
            },
            error: function () {
                dormitory.notify('Has an error in load Service', 'error');
            }
        });
    }
    function getServiceOptions(selectedId) {
        var services = "<select class='form-control ddlService'>";
        $.each(cacheObj.service, function (i, service) {
            if (selectedId === service.keyId)
                services += '<option value=" ' + service.keyId + '" selected="select">' + service.nameService + '</option>';
            else
                services += '<option value=" ' + service.keyId + '" >' + service.nameService + '</option>';
        });
        services += "</select>";
        return services;
    }
   
}