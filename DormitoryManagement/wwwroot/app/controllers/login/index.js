﻿var loginController = function () {
    this.initializer = function () {
        registerEvent();

    };
    var registerEvent = function () {
        $('#frmLogin').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'en',
            rules: {
                userName: { // Name cua input
                    required: true
                },
                password: {
                    required: true
                }
            }
        })
   
        $('#btnLogin').on('click', function (e) {
            if ($('#frmLogin').valid()) {
                e.preventDefault();
                var user = $('#txtUserName').val();
                var password = $('#txtPassword').val();
                login(user, password);
            }
 
        });
        $('form').on('keypress', function (e) {
            if ($('#frmLogin').valid()) {
                if (e.keyCode === 13) {
                    var user = $('#txtUserName').val();
                    var pass = $('#txtPassword').val();
                    login(user, pass);
                }
            }
        });
        $('#logOut').on('click', function (e) {
            var editUrl = "Admin/Account/LogOut";
            $.get(editUrl).done(function (data) { })

        });

    };
    var login = function (user, pass) {
        $.ajax({
            type: 'POST',
            data: {
                UserName: user,
                Password: pass
            },
            dataType: 'json',
            url: '/admin/login/authen',
            success: function (res) {
                if (res.success) {
                    window.location.href = "/Home/Index";
                }
                else {
                    dormitory.notify('User/Password not correct ', 'error');
                }
            }
        });
    }
}