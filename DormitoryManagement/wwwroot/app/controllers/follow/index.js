﻿var followController = function () {
    this.initialize = function () {
        loadData();
        registerEvents();
    };

    function registerEvents() {
        $('#ddlShowPage').on('change', function () {
            dormitory.config.pageSize = $(this).val();
            dormitory.config.pageIndex = 1;
            loadData(true);
        });
        $('#txtKeyword').on('keyup', function (e) {
            if (e.keyCode === 13) {
                loadData();
            }
        });
        $('#Search').on('click', function (e) {
            loadData();
        });
       
        var placeholderElement = $('#modal-placeholder-diagram');
        var url = placeholderElement.data('url');
        $.get(url).done(function (data) {
            placeholderElement.html(data);
        });
        $('#btnCreate').click(function () {
            $('#txtkeyId').val('');
            $('#txtIdError').val('');
            $('#txtIdStudent').val('');         
            $('#modal-add-edit').modal('show');
        });
        $('body').on('click', '.editRoom', function (e) {
            e.preventDefault();
            var that = $(this).attr('data-id');
            $.ajax({
                type: "GET",
                url: "/Diagrams/GetById",
                data: { id: that },
                dataType: "json",
                beforeSend: function () {
                    dormitory.startLoading();
                },
                success: function (response) {
                    console.log("data", response);
                    var data = response;
                    $('#addContactLabel').html("Edit Room");
                    $('#txtkeyId').val(data.keyId);
                    $('#txtName').val(data.name);
                    $('#txtZone').val(data.idzone);
                    $('#txtCategory').val(data.idcategory);
                    $('#txtEmployee').val(data.idemployee);
                    $('#Status').val(data.status);
                    $('#modal-add-edit').modal('show');
                    dormitory.stopLoading();
                },
                error: function (status) {
                    dormitory.notify('Có lỗi xảy ra', 'error');
                    dormitory.stopLoading();
                }
            });

        });


        $('#btnSave').click(function (e) {
            if ($('#formRoom').valid()) {
                e.preventDefault();
                var keyId;
                if ($('#txtkeyId').val() === "") {
                    keyId = 0;
                }
                else {
                    keyId = parseInt($('#txtkeyId').val());
                }
                var statusFK;
                var idcategory = $('#txtCategory').val();
                var idzone = $('#txtZone').val();
                var idemployee = $('#txtEmployee').val();
                var name = $('#txtName').val();
                $.ajax({
                    type: 'POST',
                    url: '/Diagrams/SaveEntity',
                    data: {
                        KeyId: keyId,
                        Idcategory: idcategory,
                        Idzone: idzone,
                        Idemployee: idemployee,
                        Status: 0,
                        Name: name,
                        Reservation: 0,
                    },
                    dataType: "json",
                    beforeSend: function () {
                        dormitory.startLoading();
                    },
                    success: function (response) {
                        console.log(response);
                        $('#modal-add-edit').modal('hide');
                        dormitory.notify('Success !', 'success');
                        $('#formRoom').trigger('reset');
                        dormitory.stopLoading();
                        loadData();
                    },
                    error: function (err) {
                        dormitory.notify('Cannot load data !', 'error');
                        dormitory.stopLoading();

                    },
                });
                return false;
            }
        });


        // Delete
        $('body').on('click', '.delRoom', function (e) {
            var id = $(this).attr("data-id");
            const swalWithBootstrapButtons = Swal.mixin({
                buttonsStyling: true
            });

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                confirmButtonColor: "#5cb85c",
                cancelButtonText: 'No, cancel!',
                cancelButtonColor: "#d9534f",
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: "POST",
                        url: "/Diagrams/Delete",
                        data: { id: id },
                        //headers: { "RequestVerificationToken": $('#hiddenForm input[name="__RequestVerificationToken"]').val() },
                        success: function () {
                            //table.row(tableRow).remove().draw();
                            swalWithBootstrapButtons.fire(
                                'Deleted!',
                                'Record has been deleted!',
                                'success'
                            );
                            loadData(true);
                        },
                        error: function (data, status, error) {
                            alert('error');
                            console.log(status);
                        }
                    });
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Nothing changed!',
                        'error'
                    );
                }
            });
        });
    }

    function loadData(isPageChanged) {

        var template = $('#table-template').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {               
                //status: $('#ddlStatus').val(),
                keyword: $('#txtKeyword').val(),
                page: dormitory.config.pageIndex,
                pageSize: dormitory.config.pageSize
            },
            url: '/Follows/GetAllPaging',
            dataType: 'json',
            beforeSend: function () {
                dormitory.startLoading();
            },
            success: function (response) {
            
                $.each(response.results, function (i, item) {      
                    console.log(response.results);
                    render += Mustache.render(template, {
                        Date: moment(item.date).format("DD/MM/YYYY"),
                        Student: item.idstudentNavigation.name,
                        Error: item.iderrorNavigation.description,
                        Count: item.count,
                        IDStudent: item.idstudentNavigation.idStudent
                        });
                });
               
                $('#lblTotalRecords').text(response.rowCount);
                $('#tbl-content').html(render);
              
                dormitory.stopLoading();
                wrapPaging(response.rowCount, function () {
                    loadData();
                }, isPageChanged);

            },
            error: function (status) {
                console.log(status);
                dormitory.notify('Cannot loading data', 'error');

            }
        });

    }
    function wrapPaging(recordCount, callBack, changePageSize) {
        var totalsize = Math.ceil(recordCount / dormitory.config.pageSize);
        //Unbind pagination if it existed or click change pagesize
        if ($('#paginationUL a').length === 0 || changePageSize === true) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
        //Bind Pagination Event
        if (totalsize > 0)
            $('#paginationUL').twbsPagination({
                totalPages: totalsize,
                visiblePages: 7,
                first: 'First',
                prev: 'Previous',
                next: 'Next',
                last: 'Last',
                onPageClick: function (event, p) {
                    dormitory.config.pageIndex = p;
                    setTimeout(callBack(), 200);
                }
            });
    }

    function loadCategories() {
        $.ajax({
            type: 'GET',
            data: {},
            url: '/Diagrams/GetAllCategory',
            dataType: 'json',
            beforeSend: function () {
                dormitory.startLoading();
            },
            success: function (response) {
                var render = "<option value=''>--Choose Category--</option>";

                $.each(response, function (i, item) {
                    render += "<option id='selectCate' value='" + item.keyId + "'>" + item.description + "</option>";
                });
                $('#ddlCategorySearch').html(render);
            },
            error: function (status) {
                console.log(status);
                dormitory.notify('Cannot loading category data', 'error');
            }
        });
    }


}
