﻿$(document).ready(function () {   
	//Modal
	var placeholderElement = $('#modal-placeholder-student');
	var url = placeholderElement.data('url');
	$.get(url).done(function (data) {
		placeholderElement.html(data);
    });

	$('#createSinhVien').click(function () {
		$('#KeyId').prop('readonly', false).val('');
		$('#DOB').val('');
		$('#Name').val('');
		$('#Class').val('');
		$('#PhoneNumber').val('');
		$('Teacher').val('');
        $('Idroom').val('');
		$('Email').val('');
		$('Idcard').val('');
		$('#formStudent').attr('action', "/Students/Create");
		placeholderElement.find('.modal').modal('show');
    });
    // Detail
    $('#TableSinhVien').on('click', 'tbody .detailStudent', (function () {
        var table = $('#TableSinhVien').DataTable();
        var tableRow = $(this).closest('tr');
        var id = table.row(tableRow).id();
        //var detailUrl = "/Students/Details/" + id;      
        var editUrl = "Students/Edit/" + id;
        $.get(editUrl).done(function (data) {
            window.location.href = editUrl;
            $('#editStudent').attr('action', editUrl);
            $('#editImg').attr('action', editUrl);
            var fd = new FormData();
            var files = $('#Image')[0].files[0];
            fd.append('file', files);
            $('#Image').val(fd);	

        });      
        
    }));   
	// Delete
	$('#TableSinhVien').on('click', 'tbody .deleteStudent', (function () {
		var table = $('#TableSinhVien').DataTable();
		var tableRow = $(this).closest('tr');
        var id = table.row(tableRow).id();
        console.log(id);
        const swalWithBootstrapButtons = Swal.mixin({
            buttonsStyling: true
		});

		swalWithBootstrapButtons.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, delete it!',
			confirmButtonColor: "#5cb85c",
			cancelButtonText: 'No, cancel!',
			cancelButtonColor: "#d9534f",
			reverseButtons: true
		}).then((result) => {
			if (result.value) {
				$.ajax({
					method: "POST",
					url: "/Students/Delete",
					data: { id: id },
					//headers: { "RequestVerificationToken": $('#hiddenForm input[name="__RequestVerificationToken"]').val() },
					success: function (s) {
						table.row(tableRow).remove().draw();
						swalWithBootstrapButtons.fire(
							'Deleted!',
							'Record has been deleted!',
							'success'
                        );
                       
					},
					error: function (data, status, error) {
						alert('error');
						console.log(status);
					}
				});
			} else if (
				// Read more about handling dismissals
				result.dismiss === Swal.DismissReason.cancel
			) {
				swalWithBootstrapButtons.fire(
					'Cancelled',
					'Nothing changed!',
					'error'
				);
			}
		});
    }));

});