﻿$(document).ready(function () {
    $("#TableSinhVien").DataTable({

        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once,
        "searchDelay": 1000,
        rowId: 'keyId',
        "lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]],
        "ajax": {
            "url": "/Students/LoadDataWithPaging",
            "type": "POST",
            "datatype": "json"
        },
        //"columnDefs":
        //	[{
        //		"targets": [0],
        //		"visible": false,
        //		"searchable": false,

        //	}],
        "columns": [
			{ data: "idStudent", "name": "masv", "autoWidth": true },
			{ data: "name", "name": "hoten", "autoWidth": true },
			{ data: "class", "name": "lop", "autoWidth": true },
			{ data: "phoneNumber", "name": "sdt", "autoWidth": true },		
			{ data: "faculty", "name": "maphong", "autoWidth": true },			
			{ data: "email", "name": "email", "autoWidth": true },
			{ data: "idcard", "name": "cmnd", "autoWidth": true },		
			{
                "render": function (data, type, row) {
                    return '<img src="images/Icon/info.png" class="detailStudent"  data-id=' + row.keyId + '">' +
                        "<img  src='images/Icon/delete.png' class='deleteStudent'  data-id='" + row.keyId + "';>" 
						;
				}              
                
			}
		]
	});

});