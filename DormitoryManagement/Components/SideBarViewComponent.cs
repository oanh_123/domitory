﻿using Dormitory.Business.Interface;
using Dormitory.Entity.Data;
using Dormitory.Ultilities.Constant;
using DormitoryManagement.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DormitoryManagement.Components
{
    public class SideBarViewComponent : ViewComponent
    {
        private IFunction _function;

        public SideBarViewComponent(IFunction function)
        {
            _function = function;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var roles = ((ClaimsPrincipal)User).GetSpecificClaim("Role");
            List<Function> functions;
            if (roles.Contains(CommonConstant.AdminRole))
            {
                functions = await _function.GetAll();

            }
            else
            {
                // TOTO Get by permission
                functions = new List<Function>();
            }
            return View(functions);
        }
    }
}
