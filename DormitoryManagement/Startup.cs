﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Dormitory.Business.AutoMapper;
using Dormitory.Business.Interface;
using Dormitory.Business.Service;
using Dormitory.Entity;
using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using Dormitory.Repositories.ServiceRepositories;
using DormitoryManagement.Helper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DormitoryManagement
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.Configure<CookiePolicyOptions>(options =>
            //{
            //    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
            //    options.CheckConsentNeeded = context => true;
            //    options.MinimumSameSitePolicy = SameSiteMode.None;
            //});
            string assemblyName = typeof(DormitoryContext).Namespace;
            services.AddDbContext<DormitoryContext>(options =>
           {
           options.UseSqlServer(Configuration.GetConnectionString("QLKTXConnectionStrings"),
                   o => o.MigrationsAssembly(typeof(Startup).Assembly.FullName));
           });
            services.AddIdentity<AppUser, AppRole>()
                .AddEntityFrameworkStores<DormitoryContext>()
                .AddDefaultTokenProviders();
            // Configure Identity
            services.Configure<IdentityOptions>(options =>
            {
                //Password Setting
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;

                // Lockout Setting
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;

                // User setting
                options.User.RequireUniqueEmail = true;
            });
            //automapper/
            services.AddAutoMapper(typeof(Startup));
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<DomainToViewModelMappingProfile>();
            });

            services.AddScoped<UserManager<AppUser>, UserManager<AppUser>>();
            services.AddScoped<RoleManager<AppRole>, RoleManager<AppRole>>();
            services.AddTransient<DbInitializer>();
            services.AddScoped<IUserClaimsPrincipalFactory<AppUser>, CustomClaimsPrincipalFactory>();
            // Cau hinh automapper cho dot.net core
            services.AddSingleton(Mapper.Configuration);
            services.AddScoped<IMapper>(sp => new Mapper(sp.GetRequiredService<AutoMapper.IConfigurationProvider>(), sp.GetService));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddTransient(typeof(IUnitOfWork), typeof(UnitOfWork));
            //Repositories


            // khai bao cac service viet them ( voi interface <--> class)
            services.AddTransient(typeof(IRepository<,>), typeof(Repository<,>));
            //Repositories
            services.AddTransient<IFunctionRepository, FunctionRepository>();
            services.AddTransient<IDiagramRepository, DiagramRepository>();
            services.AddTransient<ICategoryRoomRepository, CategoryRoomRepository>();
            services.AddTransient<IInvoiceRepository, InvoiceRepository>();
            services.AddTransient<IServiceRepository, ServiceRepository>();
            services.AddTransient<IDetailRepository, DetailRepository>();
            services.AddTransient<IFollowRepository, FollowRepository>();
            services.AddTransient<IRuleRepository, RuleRepository>();

            //Service 
            services.AddTransient<IFunction, FunctionService>();
            services.AddTransient<IDiagram, DiagramService>();
            services.AddTransient<ICategoryRoom, CategoryRoomService>();
            services.AddTransient<IInvoice, InvoiceService>();
            services.AddTransient<IService, ServiceService>();
            services.AddTransient<IDetailInvoice, DetailInvoiceService>();
            services.AddTransient<IFollow, FollowService>();
            services.AddTransient<IRule, RuleService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile("Logs/Dormitory-{Date}.txt");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            routes.MapRoute(
                name: "areaRoute",
                template: "{area:exists}/{controller=Login}/{action=Index}/{id?}");
                    
            });
        }
    }
}
