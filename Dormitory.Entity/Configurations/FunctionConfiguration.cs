﻿using Dormitory.Entity.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using static Dormitory.Entity.Extensions.ModelBuilderExtension;

namespace Dormitory.Entity.Configurations
{
    public class FunctionConfiguration : DbEntityConfiguration<Function>
    {
        public override void Configure(EntityTypeBuilder<Function> entity)
        {
            entity.HasKey(c => c.KeyId);
            entity.Property(c => c.KeyId).IsRequired()
            .HasColumnType("varchar(128)");
            // etc.
        }

    }
}
