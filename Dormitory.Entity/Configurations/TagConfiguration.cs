﻿using Dormitory.Entity.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using static Dormitory.Entity.Extensions.ModelBuilderExtension;

namespace Dormitory.Entity.Configurations
{
    public class TagConfiguration : DbEntityConfiguration<AppUser>
    {        

        public override void Configure(EntityTypeBuilder<AppUser> entity)
        {
            entity.Property(c => c.Id).HasMaxLength(50)
                .IsRequired().HasColumnType("varchar(50");
        }
    }
}
