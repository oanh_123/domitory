﻿using Dormitory.Business.Interface;
using Dormitory.Ultilities.Interface;
using Dormitory.Ultilities.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dormitory.Entity.Data
{
    [Table("CategoryRoom")]
    public class CategoryRoom : DomainEntity<int> , IDateTracking
    {
        public CategoryRoom()
        {
            Room = new HashSet<Room>();

        }
        public CategoryRoom(int id, decimal unitPrice, string description, int amount,DateTime? dateCreated , DateTime? dateModified)
        {
            this.KeyId = id;
            this.UnitPrice = unitPrice;
            this.Description = description;
            this.Amount = amount;
            this.DateCreated = dateCreated;
            this.DateCreated = dateModified;
        }

        [StringLength(128)]
        public string Description { set; get; }
        public decimal UnitPrice { set; get; }
        public int Amount { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }


        public virtual ICollection<Room> Room { get; set; }
       
    }
}
