﻿using Dormitory.Business.Interface;
using Dormitory.Ultilities.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dormitory.Entity.Data
{
    [Table("Zone")]
    public class Zone : DomainEntity<int>, IDateTracking
    {
        public Zone()
        {
            Room = new HashSet<Room>();
        }
        public Zone(int id, string name, DateTime? dateCreated, DateTime? dateModified)
        {
            this.KeyId = id;
            this.Name = name;
            this.DateCreated = dateCreated;
            this.DateModified = dateModified;

        }
        public string Name { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }

        public virtual ICollection<Room> Room { get; set; }
      
    }
}
