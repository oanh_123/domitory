﻿using Dormitory.Business.Interface;
using Dormitory.Ultilities.Enum;
using Dormitory.Ultilities.Interface;
using Dormitory.Ultilities.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dormitory.Entity.Data
{
    [Table("Invoice")]
    public class Invoice : DomainEntity<int>, ISwitchable, IDateTracking
    {
        public Invoice()
        {
        }
        public Invoice(int id, int idRoom, int idEmployee, DateTime date, decimal totalMoney,Status status,
             string name, DateTime? dateCreated, DateTime? dateModified)
        {
            this.KeyId = id;
            this.Idroom = idRoom;
            this.Idemployee = idEmployee;
            this.Date = date;
            this.TotalMoney = totalMoney;
            this.Name = name;
            this.Status = status;
            this.DateCreated = dateCreated;
            this.DateModified = dateModified;
        }


        public int? Idroom { get; set; }
        public int? Idemployee { get; set; }
        public DateTime? Date { get; set; }
        public decimal? TotalMoney { get; set; }
        public Status Status { get; set; }
        public string Name { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }

        public Employee IdemployeeNavigation { get; set; }
        public Room IdroomNavigation { get; set; }     
        public List<DetailInvoice> DetailInvoice { get; set; }
      
    }
}

