﻿using Dormitory.Business.Interface;
using Dormitory.Ultilities.Enum;
using Dormitory.Ultilities.Interface;
using Dormitory.Ultilities.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Dormitory.Entity.Data
{
    [Table("Function")]
    public class Function : DomainEntity<string>, ISwitchable, ISortable ,IDateTracking
    {
        public Function()
        {

        }
        public Function(string id, string name, string url,Status status, string parentId, string iconCss, int sortOrder,DateTime? dateCreated,
            DateTime? dateModified)
        {
            this.KeyId = id;
            this.Name = name;
            this.URL = url;
            this.ParentId = parentId;
            this.IconCss = iconCss;
            this.SortOrder = sortOrder;
            this.Status = status;
            this.DateCreated = dateCreated;
            this.DateModified = dateModified;
        }
        [Required]
        [StringLength(128)]
        public string Name { set; get; }

        [Required]
        [StringLength(250)]
        public string URL { set; get; }


        [StringLength(128)]
        public string ParentId { set; get; }

        public string IconCss { get; set; }
        public int SortOrder { set; get; }
        public Status Status { set; get; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}
