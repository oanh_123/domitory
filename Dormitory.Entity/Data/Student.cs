﻿using Dormitory.Business.Interface;
using Dormitory.Ultilities.Enum;
using Dormitory.Ultilities.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dormitory.Entity.Data
{
    [Table("Student")]
    public partial class Student : DomainEntity<int>, IDateTracking
    {
        public Student()
        {
            Contract = new HashSet<Contract>();
            Follow = new HashSet<Follow>();
        }
        public Student(int id ,string idStudent, string name ,Gender gender,DateTime dob,string faculty,
            string Class, string phoneNumber,string address,string phoneParents,string policyArea,
            string teacher , int idRoom, string email, string idCard, string image,string addressOfParents,
            DateTime? dateCreated , DateTime? dateModified)
        {
            this.KeyId = id;
            this.IdStudent = idStudent;
            this.Name = name;
            this.Gender = gender;
            this.DOB = dob;
            this.Faculty = faculty;
            this.Class = Class;
            this.PhoneNumber = phoneNumber;
            this.Address = address;
            this.PhoneParents = phoneParents;
            this.AddressOfParents = addressOfParents;
            this.PolicyArea = policyArea;
            this.Teacher = teacher;
            this.Idroom = idRoom;
            this.Email = email;
            this.Idcard = idCard;
            this.Image = image;
            this.DateCreated = dateCreated;
            this.DateModified = dateModified;
        }

        [DisplayName("ID Student")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter valid Number")]
        [Required(ErrorMessage = "ID Student is required")]
        public string IdStudent { get; set; }
        [Required(ErrorMessage = "Name is required")]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required(ErrorMessage = "Gender is required")]
        public Gender Gender { get; set; }
        [DisplayName("Date of birth")]
        [Required(ErrorMessage = "Date of birth is required")]
        public DateTime? DOB { get; set; }
        [MaxLength(50)]
        public string Faculty { get; set; }
        [MaxLength(8)]
        public string Class { get; set; }
        [DisplayName("Phone Number")]
        [Required(ErrorMessage = "Phone number is required")]
        [MaxLength(10)]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter valid Number")]
        public string PhoneNumber { get; set; }
        [MaxLength(100)]
        public string Address { get; set; }
        [DisplayName("Phone Number of Parents")]
        [MaxLength(10)]
        [Required(ErrorMessage = "Phone number of parents is required")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter valid Number")]
        public string PhoneParents { get; set; }
        public string PolicyArea { get; set; }
        [DisplayName("Email of Teacher")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail of teacher is not valid")]
        [Required(ErrorMessage = "Email of teacher is required")]
        [MaxLength(50)]
        public string Teacher { get; set; }
        [Required(ErrorMessage ="Room is required")]
        public int Idroom { get; set; }
        [Required(ErrorMessage = "Email  is required")]
        [MaxLength(50)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }
        [DisplayName("Identity Card")]
        [Required(ErrorMessage = "Identity Card is required")]
        [MaxLength(9)]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter valid Number")]
        public string Idcard { get; set; }
        public string Image { get; set; }
        public string AddressOfParents { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }

        public virtual Room IdroomNavigation { get; set; }
        public virtual ICollection<Contract> Contract { get; set; }
        public virtual  ICollection<Follow> Follow { get; set; }
       
    }
}
