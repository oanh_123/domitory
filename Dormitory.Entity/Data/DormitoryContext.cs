﻿using System;
using System.IO;
using System.Linq;
using Dormitory.Business.Interface;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace Dormitory.Entity.Data
{
    public partial class DormitoryContext : IdentityDbContext<AppUser, AppRole, Guid>
    {
        public DormitoryContext()
        {
        }

        public DormitoryContext(DbContextOptions<DormitoryContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AppRole> AppRole { get; set; }       
        public virtual DbSet<AppUser> AppUsers { get; set; }     
        public virtual DbSet<CategoryRoom> CategoryRoom { get; set; }
        public virtual DbSet<Contract> Contract { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Follow> Follow { get; set; }
        public virtual DbSet<Function> Functions { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<DetailInvoice> DetailInvoice { get; set; }
        public virtual DbSet<Room> Room { get; set; }
        public virtual DbSet<Rules> Rules { get; set; }
        public virtual DbSet<Service> Service { get; set; }
        public virtual DbSet<Student> Student { get; set; }
        public virtual DbSet<Zone> Zone { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-KAPTPGG\\SQLEXPRESS;Initial Catalog=Dormitory;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Identity Config
            modelBuilder.Entity<IdentityUserClaim<Guid>>().ToTable("AppUserClaims").HasKey(x => x.Id);
            modelBuilder.Entity<IdentityRoleClaim<Guid>>().ToTable("AppRoleClaims").HasKey(x => x.Id);
            modelBuilder.Entity<IdentityUserLogin<Guid>>().ToTable("AppUserLogins").HasKey(x => x.UserId);
            modelBuilder.Entity<IdentityUserRole<Guid>>().ToTable("AppUserRole").HasKey(x => new { x.RoleId, x.UserId });
            modelBuilder.Entity<IdentityUserToken<Guid>>().ToTable("AppUserTokens").HasKey(x => new { x.UserId });
            #endregion Identity Config
            modelBuilder.Entity<CategoryRoom>(entity =>
            {
                entity.HasKey(e => e.KeyId);

                entity.Property(e => e.KeyId).HasColumnName("IDCategory");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.UnitPrice).HasColumnType("money");
            });

            modelBuilder.Entity<Contract>(entity =>
            {
                entity.HasKey(e => e.KeyId);

                entity.Property(e => e.KeyId)
                    .HasColumnName("IDContracts");

                entity.Property(e => e.DateEnd).HasColumnType("date");

                entity.Property(e => e.DateStart).HasColumnType("date");

                entity.Property(e => e.Idemployee).HasColumnName("IDEmployee");

                entity.Property(e => e.Idstudent).HasColumnName("IDStudent");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.Contract)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_Contract_Employee");
             
            
                entity.HasOne(d => d.IdstudentNavigation)
                    .WithMany(p => p.Contract)
                    .HasForeignKey(d => d.Idstudent)
                    .HasConstraintName("FK_Contract_Student");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.KeyId);

                entity.Property(e => e.KeyId).HasColumnName("IDEmployee");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Position).HasMaxLength(50);
            });
          
            modelBuilder.Entity<Follow>(entity =>
            {
                entity.HasKey(e => e.KeyId);

                entity.Property(e => e.KeyId).HasColumnName("IDFollower");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Iderror).HasColumnName("IDError");

                entity.Property(e => e.Idstudent).HasColumnName("IDStudent");

                entity.HasOne(d => d.IderrorNavigation)
                    .WithMany(p => p.Follow)
                    .HasForeignKey(d => d.Iderror)
                    .HasConstraintName("FK_Follow_Rules");

                entity.HasOne(d => d.IdstudentNavigation)
                    .WithMany(p => p.Follow)
                    .HasForeignKey(d => d.Idstudent)
                    .HasConstraintName("FK_Follow_Student");
            });

            modelBuilder.Entity<Function>(entity =>
            {
                entity.HasKey(c => c.KeyId);

                entity.Property(c => c.KeyId).IsRequired()

                .HasColumnType("varchar(128)");
            });
            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.HasKey(e => e.KeyId);

                entity.Property(e => e.KeyId).HasColumnName("IDInvoices");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Idemployee).HasColumnName("IDEmployee");

                entity.Property(e => e.Idroom).HasColumnName("IDRoom");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.TotalMoney).HasColumnType("money");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.Invoice)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_Invoice_Employee");

                entity.HasOne(d => d.IdroomNavigation)
                    .WithMany(p => p.Invoice)
                    .HasForeignKey(d => d.Idroom)
                    .HasConstraintName("FK_Invoice_Room");

            });
            modelBuilder.Entity<DetailInvoice>(entity =>
            {
                entity.HasKey(e => e.KeyId);

                entity.Property(e => e.KeyId).HasColumnName("IDDetailInvoices");

                entity.Property(e => e.Number).HasColumnName("Number");             

                entity.HasOne(d => d.InvoiceFKNavigation)
                    .WithMany(p => p.DetailInvoice)
                    .HasForeignKey(d => d.InvoiceFK)
                    .HasConstraintName("FK_DetailInvoice_Invoice");

                entity.HasOne(d => d.ServiceFKNavigation)
                    .WithMany(p => p.DetailInvoice)
                    .HasForeignKey(d => d.ServiceFK)
                    .HasConstraintName("FK_DetailInvoice_Service");

            });

            modelBuilder.Entity<Room>(entity =>
            {
                entity.HasKey(e => e.KeyId);

                entity.Property(e => e.KeyId).HasColumnName("KeyId");

                entity.Property(e => e.Idcategory).HasColumnName("IDCategory");

                entity.Property(e => e.Idemployee).HasColumnName("IDEmployee");

                entity.Property(e => e.Idzone).HasColumnName("IDZone");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.IdcategoryNavigation)
                    .WithMany(p => p.Room)
                    .HasForeignKey(d => d.Idcategory)
                    .HasConstraintName("FK_Room_CategoryRoom");

                entity.HasOne(d => d.IdemployeeNavigation)
                    .WithMany(p => p.Room)
                    .HasForeignKey(d => d.Idemployee)
                    .HasConstraintName("FK_Room_Employee");

                entity.HasOne(d => d.IdzoneNavigation)
                    .WithMany(p => p.Room)
                    .HasForeignKey(d => d.Idzone)
                    .HasConstraintName("FK_Room_Zone");
            });

            modelBuilder.Entity<Rules>(entity =>
            {
                entity.HasKey(e => e.KeyId);

                entity.Property(e => e.KeyId).HasColumnName("IDError");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.Punish).HasMaxLength(50);
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.HasKey(e => e.KeyId);

                entity.Property(e => e.KeyId).HasColumnName("IDService");

                entity.Property(e => e.NameService).HasMaxLength(50);

                entity.Property(e => e.UnitPrice).HasColumnType("money");
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.HasKey(e => e.KeyId);

                entity.Property(e => e.KeyId).HasColumnName("KeyID");

                entity.Property(e => e.IdStudent).HasColumnName("IDStudent");

                entity.Property(e => e.DOB).HasColumnName("DateOfBirth");

                entity.Property(e => e.Faculty).HasMaxLength(50);

                entity.Property(e => e.Address).HasMaxLength(100);

                entity.Property(e => e.Class).HasMaxLength(10);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Idcard).HasColumnName("IDCard");
          
                entity.Property(e => e.Idroom).HasColumnName("IDRoom");

                entity.Property(e => e.Image).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Teacher).HasMaxLength(50);
                             
                entity.HasOne(d => d.IdroomNavigation)
                    .WithMany(p => p.Student)
                    .HasForeignKey(d => d.Idroom)
                    .HasConstraintName("FK_Student_Room");
            });

            modelBuilder.Entity<Zone>(entity =>
            {
                entity.HasKey(e => e.KeyId);

                entity.Property(e => e.KeyId).HasColumnName("IDZone");

                entity.Property(e => e.Name).HasMaxLength(50);
            });
        }
        public override int SaveChanges()
        {
            var modified = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified || e.State == EntityState.Added);
            foreach (EntityEntry item in modified)
            {
                var ChangeOrAddedItem = item.Entity as IDateTracking;
                if (ChangeOrAddedItem != null)
                {
                    if (item.State == EntityState.Added)
                    {
                        ChangeOrAddedItem.DateCreated = DateTime.Now;
                    }
                    ChangeOrAddedItem.DateModified = DateTime.Now;

                }
            }
            return base.SaveChanges();
        }
        public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DormitoryContext>
        {
            public DormitoryContext CreateDbContext(string[] args)
            {
                Microsoft.Extensions.Configuration.IConfiguration configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json").Build();
                var builder = new DbContextOptionsBuilder<DormitoryContext>();
                var connectionString = configuration.GetConnectionString("QLKTXConnectionStrings");
                builder.UseSqlServer(connectionString);
                return new DormitoryContext(builder.Options);
            }
        }
    }
}
