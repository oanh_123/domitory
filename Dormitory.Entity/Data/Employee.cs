﻿using Dormitory.Business.Interface;
using Dormitory.Ultilities.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dormitory.Entity.Data
{
    [Table("Employee")]
    public class Employee :DomainEntity<int> , IDateTracking
    {
        public Employee()
        {
            Contract = new HashSet<Contract>();
            Invoice = new HashSet<Invoice>();
            Room = new HashSet<Room>();
        }
        public Employee(int id , string name , int phoneNumber , string email, string position, DateTime? dateCreated, DateTime? dateModified)
        {
            this.KeyId = id;
            this.Name = name;
            this.PhoneNumber = phoneNumber;
            this.Email = email;
            this.Position = position;
            this.DateCreated = dateCreated;
            this.DateModified = dateModified;
        }

        public string Name { get; set; }
        public int? PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Position { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }

        public virtual ICollection<Contract> Contract { get; set; }
        public virtual ICollection<Invoice> Invoice { get; set; }
        public virtual ICollection<Room> Room { get; set; }
      
    }
}
