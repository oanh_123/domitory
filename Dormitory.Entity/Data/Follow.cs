﻿using Dormitory.Business.Interface;
using Dormitory.Ultilities.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dormitory.Entity.Data
{
    [Table("Follow")]
    public  class Follow :DomainEntity<int>, IDateTracking
    {
        public Follow() { }
        public Follow( int id, int idStudent, int idError, int count , DateTime date, DateTime? dateCreated, DateTime? dateModified)
        {
            this.KeyId = id;
            this.Idstudent = idStudent;
            this.Iderror = idError;
            this.Count = count;
            this.Date = date;
            this.DateCreated = dateCreated;
            this.DateModified = dateModified;
        }
        public int? Idstudent { get; set; }
        public int? Iderror { get; set; }
        public int? Count { get; set; }
        public DateTime? Date { get; set; }

        public Rules IderrorNavigation { get; set; }
        public Student IdstudentNavigation { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}
