﻿using Dormitory.Business.Interface;
using Dormitory.Ultilities.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Dormitory.Entity.Data
{
    [Table("DetailInvoice")]
    public class DetailInvoice : DomainEntity<int>
    {
        public DetailInvoice()
        {

        }
        public DetailInvoice(int id , int invoiceFK, int serviceFK, int number , decimal money)
        {
            this.KeyId = id;
            this.InvoiceFK = InvoiceFK;
            this.ServiceFK = serviceFK;
            this.Number = number;
            this.Money = money;
        }
        public int InvoiceFK { get; set; }
        public int ServiceFK {get;set;}
        public int Number { get; set; }
        public decimal Money { get; set; }

        public virtual Invoice InvoiceFKNavigation { get; set; }
        public virtual Service ServiceFKNavigation { get; set; }
    }
}
