﻿using Dormitory.Business.Interface;
using Dormitory.Ultilities.Enum;
using Dormitory.Ultilities.Interface;
using Dormitory.Ultilities.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dormitory.Entity.Data
{
    [Table("Room")]
    public  class Room :DomainEntity<int>, IDateTracking
    {
        
        public Room()
        {
            //ContractIdfeeNavigation = new HashSet<Contract>();
            //ContractIdroomNavigation = new HashSet<Contract>();
            //Fee = new HashSet<Fee>();
            //Invoice = new HashSet<Invoice>();
            //Student = new HashSet<Student>();
        }
        public Room(int id, int idCategory , int idEmployee , int idZone ,Condition status, string name,
            int reservation,DateTime? dateCreated, DateTime? dateModified)
        {
            this.KeyId = id;
            this.Idcategory = idCategory;
            this.Idemployee = idEmployee;
            this.Idzone = idZone;
            this.Name = name;
            this.Status = Condition.StillEmpty;
            this.DateCreated = dateCreated;
            this.DateModified = dateModified;
            this.Reservation = reservation;
            
        }

        public int? Idcategory { get; set; }
        public int? Idemployee { get; set; }
        public Condition Status { get; set; }
        public string Name { get; set; }
        public int? Idzone { get; set; }
        public int Reservation { get; set; }

        public virtual CategoryRoom IdcategoryNavigation { get; set; }
        public virtual Employee IdemployeeNavigation { get; set; }
        public virtual Zone IdzoneNavigation { get; set; }
        public ICollection<Invoice> Invoice { get; set; }
        public ICollection<Student> Student { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}
