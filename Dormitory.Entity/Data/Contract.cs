﻿using Dormitory.Business.Interface;
using Dormitory.Ultilities.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dormitory.Entity.Data
{
    [Table("Contract")]
    public class Contract :DomainEntity<int>, IDateTracking

    {
        public Contract()
        {

        }
        public Contract(int id , int idStudent, DateTime dateStart , DateTime dateEnd , int idEmployee,
            DateTime? dateCreated , DateTime? dateModified)
        {
            this.KeyId = id;
            this.Idstudent = idStudent;
            this.DateStart = dateStart;
            this.DateEnd = dateEnd;
            this.Idemployee = idEmployee;
            this.DateCreated = dateCreated;
            this.DateModified = dateModified;
        }
        public int? Idstudent { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public int? Idemployee { get; set; }

        public virtual Employee IdemployeeNavigation { get; set; }
        public virtual Student IdstudentNavigation { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}
