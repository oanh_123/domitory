﻿using Dormitory.Business.Interface;
using Dormitory.Ultilities.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dormitory.Entity.Data
{
    [Table("Service")]
    public class Service : DomainEntity<int> ,IDateTracking
    {
        public Service()
        {
        }
        public Service(int id , string nameService , decimal unitPrice, DateTime? dateCreated, DateTime? dateModified)
        {
            this.KeyId = id;
            this.NameService = nameService;
            this.UnitPrice = unitPrice;
            this.DateCreated = dateCreated;
            this.DateModified = dateModified;
        }

        public string NameService { get; set; }
        public decimal UnitPrice { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }

        public virtual ICollection<DetailInvoice> DetailInvoice { get; set; }

        
    }
}
