﻿using Dormitory.Business.Interface;
using Dormitory.Ultilities.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dormitory.Entity.Data
{
    [Table("Rules")]
    public class Rules : DomainEntity<int> , IDateTracking
    {
        public Rules()
        {
            Follow = new HashSet<Follow>();
        }
        public Rules(int id , string description , string punish, DateTime? dateCreated, DateTime? dateModified)
        {
            this.KeyId = id;
            this.Description = description;
            this.Punish = punish;
            this.DateCreated = dateCreated;
            this.DateCreated = dateModified;
        }

        public string Description { get; set; }
        public string Punish { get; set; }

        public virtual ICollection<Follow> Follow { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}
