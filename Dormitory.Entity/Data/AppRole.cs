﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dormitory.Entity.Data
{
    [Table("AppRole")]
    public class AppRole : IdentityRole<Guid>
    {
        public AppRole() : base() { }
        public AppRole(string name, string description) : base(name)
        {
            this.Description = description;
        }
        [StringLength(50)]
        public string Description { get; set; }

    }

}
