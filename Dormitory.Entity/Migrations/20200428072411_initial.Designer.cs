﻿// <auto-generated />
using System;
using Dormitory.Entity.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Dormitory.Entity.Migrations
{
    [DbContext(typeof(DormitoryContext))]
    [Migration("20200428072411_initial")]
    partial class initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.14-servicing-32113")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Dormitory.Entity.Data.AppRole", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp");

                    b.Property<string>("Description")
                        .HasMaxLength(50);

                    b.Property<string>("Name");

                    b.Property<string>("NormalizedName");

                    b.HasKey("Id");

                    b.ToTable("AppRole");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.AppUser", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("Avatar");

                    b.Property<decimal>("Balance");

                    b.Property<DateTime?>("Birthday");

                    b.Property<string>("ConcurrencyStamp");

                    b.Property<DateTime?>("DateCreated");

                    b.Property<DateTime?>("DateModified");

                    b.Property<string>("Email");

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FullName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail");

                    b.Property<string>("NormalizedUserName");

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<int>("Status");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName");

                    b.HasKey("Id");

                    b.ToTable("AppUsers");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.CategoryRoom", b =>
                {
                    b.Property<int>("KeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("IDCategory")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Amount");

                    b.Property<DateTime?>("DateCreated");

                    b.Property<DateTime?>("DateModified");

                    b.Property<string>("Description")
                        .HasMaxLength(50);

                    b.Property<decimal>("UnitPrice")
                        .HasColumnType("money");

                    b.HasKey("KeyId");

                    b.ToTable("CategoryRoom");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Contract", b =>
                {
                    b.Property<int>("KeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("IDContracts")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("DateCreated");

                    b.Property<DateTime?>("DateEnd")
                        .HasColumnType("date");

                    b.Property<DateTime?>("DateModified");

                    b.Property<DateTime?>("DateStart")
                        .HasColumnType("date");

                    b.Property<int?>("Idemployee")
                        .HasColumnName("IDEmployee");

                    b.Property<int?>("Idstudent")
                        .HasColumnName("IDStudent");

                    b.HasKey("KeyId");

                    b.HasIndex("Idemployee");

                    b.HasIndex("Idstudent");

                    b.ToTable("Contract");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.DetailInvoice", b =>
                {
                    b.Property<int>("KeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("IDDetailInvoices")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("InvoiceFK");

                    b.Property<decimal>("Money");

                    b.Property<int>("Number")
                        .HasColumnName("Number");

                    b.Property<int>("ServiceFK");

                    b.HasKey("KeyId");

                    b.HasIndex("InvoiceFK");

                    b.HasIndex("ServiceFK");

                    b.ToTable("DetailInvoice");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Employee", b =>
                {
                    b.Property<int>("KeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("IDEmployee")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("DateCreated");

                    b.Property<DateTime?>("DateModified");

                    b.Property<string>("Email")
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<int?>("PhoneNumber");

                    b.Property<string>("Position")
                        .HasMaxLength(50);

                    b.HasKey("KeyId");

                    b.ToTable("Employee");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Follow", b =>
                {
                    b.Property<int>("KeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("IDFollower")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("Count");

                    b.Property<DateTime?>("Date")
                        .HasColumnType("date");

                    b.Property<DateTime?>("DateCreated");

                    b.Property<DateTime?>("DateModified");

                    b.Property<int?>("Iderror")
                        .HasColumnName("IDError");

                    b.Property<int?>("Idstudent")
                        .HasColumnName("IDStudent");

                    b.HasKey("KeyId");

                    b.HasIndex("Iderror");

                    b.HasIndex("Idstudent");

                    b.ToTable("Follow");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Function", b =>
                {
                    b.Property<string>("KeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("varchar(128)");

                    b.Property<DateTime?>("DateCreated");

                    b.Property<DateTime?>("DateModified");

                    b.Property<string>("IconCss");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("ParentId")
                        .HasMaxLength(128);

                    b.Property<int>("SortOrder");

                    b.Property<int>("Status");

                    b.Property<string>("URL")
                        .IsRequired()
                        .HasMaxLength(250);

                    b.HasKey("KeyId");

                    b.ToTable("Function");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Invoice", b =>
                {
                    b.Property<int>("KeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("IDInvoices")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("Date")
                        .HasColumnType("date");

                    b.Property<DateTime?>("DateCreated");

                    b.Property<DateTime?>("DateModified");

                    b.Property<int?>("Idemployee")
                        .HasColumnName("IDEmployee");

                    b.Property<int?>("Idroom")
                        .HasColumnName("IDRoom");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<int>("Status");

                    b.Property<decimal?>("TotalMoney")
                        .HasColumnType("money");

                    b.HasKey("KeyId");

                    b.HasIndex("Idemployee");

                    b.HasIndex("Idroom");

                    b.ToTable("Invoice");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Room", b =>
                {
                    b.Property<int>("KeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("KeyId")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("DateCreated");

                    b.Property<DateTime?>("DateModified");

                    b.Property<int?>("Idcategory")
                        .HasColumnName("IDCategory");

                    b.Property<int?>("Idemployee")
                        .HasColumnName("IDEmployee");

                    b.Property<int?>("Idzone")
                        .HasColumnName("IDZone");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<int>("Reservation");

                    b.Property<int>("Status");

                    b.HasKey("KeyId");

                    b.HasIndex("Idcategory");

                    b.HasIndex("Idemployee");

                    b.HasIndex("Idzone");

                    b.ToTable("Room");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Rules", b =>
                {
                    b.Property<int>("KeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("IDError")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("DateCreated");

                    b.Property<DateTime?>("DateModified");

                    b.Property<string>("Description")
                        .HasMaxLength(50);

                    b.Property<string>("Punish")
                        .HasMaxLength(50);

                    b.HasKey("KeyId");

                    b.ToTable("Rules");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Service", b =>
                {
                    b.Property<int>("KeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("IDService")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("DateCreated");

                    b.Property<DateTime?>("DateModified");

                    b.Property<string>("NameService")
                        .HasMaxLength(50);

                    b.Property<decimal?>("UnitPrice")
                        .HasColumnType("money");

                    b.HasKey("KeyId");

                    b.ToTable("Service");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Student", b =>
                {
                    b.Property<int>("KeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("KeyID")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .HasMaxLength(100);

                    b.Property<string>("AddressOfParents");

                    b.Property<string>("Class")
                        .HasMaxLength(10);

                    b.Property<DateTime?>("DOB")
                        .IsRequired()
                        .HasColumnName("DateOfBirth");

                    b.Property<DateTime?>("DateCreated");

                    b.Property<DateTime?>("DateModified");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Faculty")
                        .HasMaxLength(50);

                    b.Property<int>("Gender");

                    b.Property<string>("IdStudent")
                        .IsRequired()
                        .HasColumnName("IDStudent");

                    b.Property<string>("Idcard")
                        .IsRequired()
                        .HasColumnName("IDCard")
                        .HasMaxLength(9);

                    b.Property<int>("Idroom")
                        .HasColumnName("IDRoom");

                    b.Property<string>("Image")
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("PhoneNumber")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("PhoneParents")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("PolicyArea");

                    b.Property<string>("Teacher")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("KeyId");

                    b.HasIndex("Idroom");

                    b.ToTable("Student");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Zone", b =>
                {
                    b.Property<int>("KeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("IDZone")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("DateCreated");

                    b.Property<DateTime?>("DateModified");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.HasKey("KeyId");

                    b.ToTable("Zone");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<System.Guid>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<Guid>("RoleId");

                    b.HasKey("Id");

                    b.ToTable("AppRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<System.Guid>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.ToTable("AppUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<System.Guid>", b =>
                {
                    b.Property<Guid>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("ProviderKey");

                    b.HasKey("UserId");

                    b.ToTable("AppUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<System.Guid>", b =>
                {
                    b.Property<Guid>("RoleId");

                    b.Property<Guid>("UserId");

                    b.HasKey("RoleId", "UserId");

                    b.ToTable("AppUserRole");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<System.Guid>", b =>
                {
                    b.Property<Guid>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId");

                    b.ToTable("AppUserTokens");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Contract", b =>
                {
                    b.HasOne("Dormitory.Entity.Data.Employee", "IdemployeeNavigation")
                        .WithMany("Contract")
                        .HasForeignKey("Idemployee")
                        .HasConstraintName("FK_Contract_Employee");

                    b.HasOne("Dormitory.Entity.Data.Student", "IdstudentNavigation")
                        .WithMany("Contract")
                        .HasForeignKey("Idstudent")
                        .HasConstraintName("FK_Contract_Student");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.DetailInvoice", b =>
                {
                    b.HasOne("Dormitory.Entity.Data.Invoice", "InvoiceFKNavigation")
                        .WithMany("DetailInvoice")
                        .HasForeignKey("InvoiceFK")
                        .HasConstraintName("FK_DetailInvoice_Invoice")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Dormitory.Entity.Data.Service", "ServiceFKNavigation")
                        .WithMany("DetailInvoice")
                        .HasForeignKey("ServiceFK")
                        .HasConstraintName("FK_DetailInvoice_Service")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Follow", b =>
                {
                    b.HasOne("Dormitory.Entity.Data.Rules", "IderrorNavigation")
                        .WithMany("Follow")
                        .HasForeignKey("Iderror")
                        .HasConstraintName("FK_Follow_Rules");

                    b.HasOne("Dormitory.Entity.Data.Student", "IdstudentNavigation")
                        .WithMany("Follow")
                        .HasForeignKey("Idstudent")
                        .HasConstraintName("FK_Follow_Student");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Invoice", b =>
                {
                    b.HasOne("Dormitory.Entity.Data.Employee", "IdemployeeNavigation")
                        .WithMany("Invoice")
                        .HasForeignKey("Idemployee")
                        .HasConstraintName("FK_Invoice_Employee");

                    b.HasOne("Dormitory.Entity.Data.Room", "IdroomNavigation")
                        .WithMany("Invoice")
                        .HasForeignKey("Idroom")
                        .HasConstraintName("FK_Invoice_Room");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Room", b =>
                {
                    b.HasOne("Dormitory.Entity.Data.CategoryRoom", "IdcategoryNavigation")
                        .WithMany("Room")
                        .HasForeignKey("Idcategory")
                        .HasConstraintName("FK_Room_CategoryRoom");

                    b.HasOne("Dormitory.Entity.Data.Employee", "IdemployeeNavigation")
                        .WithMany("Room")
                        .HasForeignKey("Idemployee")
                        .HasConstraintName("FK_Room_Employee");

                    b.HasOne("Dormitory.Entity.Data.Zone", "IdzoneNavigation")
                        .WithMany("Room")
                        .HasForeignKey("Idzone")
                        .HasConstraintName("FK_Room_Zone");
                });

            modelBuilder.Entity("Dormitory.Entity.Data.Student", b =>
                {
                    b.HasOne("Dormitory.Entity.Data.Room", "IdroomNavigation")
                        .WithMany("Student")
                        .HasForeignKey("Idroom")
                        .HasConstraintName("FK_Student_Room")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
