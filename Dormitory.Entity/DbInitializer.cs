﻿using Dormitory.Entity.Data;
using Dormitory.Ultilities.Constant;
using Dormitory.Ultilities.Enum;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dormitory.Entity
{
    public class DbInitializer
    {
        private readonly DormitoryContext _context;
        private UserManager<AppUser> _userManager;
        private RoleManager<AppRole> _roleManager;
        public DbInitializer (DormitoryContext context , UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;

        }             
        public async Task Seed()
        {
            if (!_roleManager.Roles.Any())
            {
                await _roleManager.CreateAsync(new AppRole()
                {
                    Name = "Admin",
                    NormalizedName = "Admin",
                    Description = "Top Manager"
                });
                await _roleManager.CreateAsync(new AppRole()
                {
                    Name = "Manager",
                    NormalizedName = "Manager",
                    Description = "Manager"
                });
                await _roleManager.CreateAsync(new AppRole()
                {
                    Name = "Staff",
                    NormalizedName = "Staff",
                    Description = "Staff"
                });
            }
            if (!_userManager.Users.Any())
            {
                await _userManager.CreateAsync(new AppUser()
                {
                    UserName = "admin",
                    FullName = "Administrator",
                    Email = "admin@gmail.com",
                    Balance = 0,
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    Status = Status.Active
                }, CommonConstant.Password);
                var user = await _userManager.FindByNameAsync("admin");
                await _userManager.AddToRoleAsync(user, "Admin");

                await _userManager.CreateAsync(new AppUser()
                {
                    UserName = "manager",
                    FullName = "Manager",
                    Email = "manager@gmail.com",
                    Balance = 0,
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    Status = Status.Active
                }, CommonConstant.PasswordManager);
                var manager = await _userManager.FindByNameAsync("manager");
                await _userManager.AddToRoleAsync(manager, "Manager");
            }

            if(_context.Zone.Count() == 0)
            {
                _context.Zone.AddRange(new List<Zone>()
                {
                    new Zone(){Name="Khu A"},
                    new Zone(){Name="Khu B"},
                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch(Exception ex)
            {
                string a = ex.ToString();
            };

            if (_context.CategoryRoom.Count() == 0)
            {
                _context.CategoryRoom.AddRange(new List<CategoryRoom>()
                {                 
                    new CategoryRoom(){Description="Phòng 4 giường",UnitPrice=800000,Amount=4},
                    new CategoryRoom(){Description="Phòng 6 giường",UnitPrice=500000,Amount=6},                    
                    new CategoryRoom(){Description="Phòng 10 giường",UnitPrice=200000,Amount=10},

                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };

            if (_context.Employee.Count() == 0)
            {
                _context.Employee.AddRange(new List<Employee>()
                {
                    new Employee(){Name="Administrator",PhoneNumber=865536436,Email="administrator@gmail.com",Position="Admin"},
                    new Employee(){Name="Hoàng Oanh",PhoneNumber=8343488,Email="hoangoanh@gmail.com",Position="Manager"},
                    new Employee(){Name="Ngọc Huyền",PhoneNumber=098848384,Email="ngochuyen@gmail.com",Position="Manager"},
                    new Employee(){Name="Tuấn Kiệt",PhoneNumber=09349499,Email="tuankiet@gmail.com",Position="Staff"},
                    new Employee(){Name="Mỹ Dung",PhoneNumber=093949349,Email="mydung@gmail.com",Position="Staff"},

                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };

            if (_context.Room.Count() == 0)
            {
                _context.Room.AddRange(new List<Room>()
                {
                    new Room(){Name="101",Idzone=1,Idcategory=3,Idemployee=2,Status=Condition.StillEmpty,Reservation=7},
                    new Room(){Name="102",Idzone=1,Idcategory=1,Idemployee=1,Status=Condition.StillEmpty,Reservation=3},
                    new Room(){Name="103",Idzone=2,Idcategory=2,Idemployee=3,Status=Condition.StillEmpty,Reservation=4},
                    new Room(){Name="104",Idzone=1,Idcategory=1,Idemployee=4,Status=Condition.StillEmpty,Reservation=4},
                    new Room(){Name="105",Idzone=2,Idcategory=1,Idemployee=3,Status=Condition.StillEmpty,Reservation=4},
                    new Room(){Name="106",Idzone=1,Idcategory=2,Idemployee=1,Status=Condition.StillEmpty,Reservation=6},
                    new Room(){Name="107",Idzone=1,Idcategory=2,Idemployee=2,Status=Condition.StillEmpty,Reservation=6},

                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };

            if (_context.Service.Count() == 0)
            {
                _context.Service.AddRange(new List<Service>()
                {
                     new Service(){NameService="Điện",UnitPrice=3000},
                     new Service(){NameService="Nước",UnitPrice=10000},
                     new Service(){NameService="Wifi",UnitPrice=50000},
                     new Service(){NameService="Gửi xe",UnitPrice=100000},
                     new Service(){NameService="Giặt ủi",UnitPrice=70000},
                     new Service(){NameService="Cơ sở vật chất",UnitPrice=150000},

                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };

            if (_context.Invoice.Count() == 0)
            {
                _context.Invoice.AddRange(new List<Invoice>()
                {
                    new Invoice(){Idroom=1,Idemployee=2,Date=DateTime.Parse("2020-01-24"),TotalMoney=270000,Status=Status.Active,Name="#HD-KTX01"},
                    new Invoice(){Idroom=2,Idemployee=2,Date=DateTime.Parse("2020-01-24"),TotalMoney=267000,Status=Status.InActive,Name="#HD-KTX02"},
                    new Invoice(){Idroom=3,Idemployee=1,Date=DateTime.Parse("2020-01-24"),TotalMoney=270000,Status=Status.Active,Name="#HD-KTX03"},
                    new Invoice(){Idroom=4,Idemployee=3,Date=DateTime.Parse("2020-01-24"),TotalMoney=356000,Status=Status.Active,Name="#HD-KTX04"},
                  
                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };
            if (_context.DetailInvoice.Count() == 0)
            {
                _context.DetailInvoice.AddRange(new List<DetailInvoice>()
                {
                    new DetailInvoice(){InvoiceFK=1 , ServiceFK=1,Number=40,Money=120000},
                    new DetailInvoice(){InvoiceFK=1 , ServiceFK=2,Number=10,Money=100000},
                    new DetailInvoice(){InvoiceFK=1 , ServiceFK=3,Number=1,Money=50000},
                    new DetailInvoice(){InvoiceFK=2 , ServiceFK=1,Number=39,Money=117000},
                    new DetailInvoice(){InvoiceFK=2 , ServiceFK=2,Number=15,Money=150000},
                    new DetailInvoice(){InvoiceFK=3 , ServiceFK=1,Number=50,Money=150000},
                    new DetailInvoice(){InvoiceFK=3 , ServiceFK=2,Number=12,Money=120000},
                    new DetailInvoice(){InvoiceFK=4 , ServiceFK=1,Number=52,Money=156000},
                    new DetailInvoice(){InvoiceFK=4 , ServiceFK=2,Number=20,Money=200000},

                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };




            if (_context.Student.Count() == 0)
            {
                _context.Student.AddRange(new List<Student>()
            {
                new Student() { Name="Thu Hà",Class="DCT1161",Teacher="tolearn8798@gmail.com",PhoneNumber="0866192456",Idroom=2,Email="ngochuyen1309@gmail.com",Idcard="023923239",Image="1.jpg",IdStudent="3116410086",Gender = Gender.Female,DOB=DateTime.Parse("1997-05-08"),Faculty="Information Technology",Address="90 Le Hong Phong",AddressOfParents="90 Le Hong Phong",PhoneParents="0866937592",PolicyArea="Con thương binh, liệt sĩ"},
                new Student() { Name="Huỳnh Hoa",Class="DCT1163",Teacher="hoangnguyen@gmail.com",PhoneNumber="0866192456",Idroom=1,Email="ngochuyen1309@gmail.com",Idcard="24344344",Image="2.jpg",IdStudent="3116410110",Gender = Gender.Male,DOB=DateTime.Parse("1999-03-08"),Faculty="Information Technology",Address="14 Ngô Đức Kế",AddressOfParents="14 Ngô Đức Kế",PhoneParents="08323322",PolicyArea=null},
                new Student() { Name="Minh Châu",Class="DCT1161",Teacher="ngochuyen1309@gmail.com",PhoneNumber="34344434",Idroom=3,Email="ngochuyen1309@gmail.com",Idcard="344422123",Image="3.jpg",IdStudent="3116410043",Gender = Gender.Female,DOB=DateTime.Parse("1998-10-12"),Faculty="Accountant",Address="30/2 Cao Thắng",AddressOfParents="30/2 Cao Thắng",PhoneParents="022232323",PolicyArea="Gia đình khó khăn"},
                new Student() { Name="Bích Lan",Class="DCT1162",Teacher="thuhuong@gmail.com",PhoneNumber="0866192456",Idroom=1,Email="tolearn8798@gmail.com",Idcard="65652233",Image="4.jpg",IdStudent="3116410005",Gender = Gender.Male,DOB=DateTime.Parse("2000-03-28"),Faculty="Information Technology",Address="44/12 Huyền Trân Công Chúa",AddressOfParents="44/12 Huyền Trân Công Chúa",PhoneParents="0866937592",PolicyArea=null},
                new Student() { Name="Mỹ Huệ",Class="DCT1164",Teacher="tolearn8798@gmail.com",PhoneNumber="0556456456",Idroom=3,Email="tolearn8798@gmail.com",Idcard="212332",Image="5.jpg",IdStudent="3116410013",Gender = Gender.Female,DOB=DateTime.Parse("1998-10-08"),Faculty="Education",Address="1 Nguyễn Thị Minh Khai",AddressOfParents="1 Nguyễn Thị Minh Khai",PhoneParents="0866937592",PolicyArea=null},
                new Student() { Name="Xuân Duyên",Class="DCT1165",Teacher="thuhuong@gmail.com",PhoneNumber="04577422",Idroom=1,Email="tolearn8798@gmail.com",Idcard="23923239",Image="6.jpg",IdStudent="3116410050",Gender = Gender.Male,DOB=DateTime.Parse("2001-01-20"),Faculty="Electronic",Address="120/1 Le Hong Phong",AddressOfParents="120/1 Le Hong Phong",PhoneParents="0866937592",PolicyArea="Gia đình khó khăn"},

                  });

            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };
            if (_context.Rules.Count() == 0)
            {
                _context.Rules.AddRange(new List<Rules>()
                {
                    new Rules(){Description="Về trễ",Punish="Nhắc nhở"},
                    new Rules(){Description="Đánh bài",Punish="Nhắc nhở"},
                    new Rules(){Description="Nấu ăn",Punish="Nhắc nhở"},
                    new Rules(){Description="Phá hoại tài sản",Punish="Gửi mail cho cố vấn"},
                    new Rules(){Description="Đóng tiền trễ hạn",Punish="Gửi mail cho cố vấn"},
                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };

            if (_context.Follow.Count() == 0)
            {
                _context.Follow.AddRange(new List<Follow>()
                {
                    new Follow(){Idstudent=3,Iderror=2,Count=1,Date=DateTime.Parse("2020-01-10")},
                    new Follow(){Idstudent=1,Iderror=1,Count=1,Date=DateTime.Parse("2020-02-19")},
                    new Follow(){Idstudent=3,Iderror=3,Count=2,Date=DateTime.Parse("2020-01-30")},
                    new Follow(){Idstudent=2,Iderror=3,Count=1,Date=DateTime.Parse("2020-04-10")},
                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };

          

            if (_context.Contract.Count() == 0)
            {
                _context.Contract.AddRange(new List<Contract>()
                {
                    new Contract(){Idstudent=1,Idemployee=3,DateStart=DateTime.Parse("2019-01-01"),DateEnd=DateTime.Parse("2019-12-31")},
                    new Contract(){Idstudent=2,Idemployee=1,DateStart=DateTime.Parse("2019-01-01"),DateEnd=DateTime.Parse("2019-12-31")},
                    new Contract(){Idstudent=3,Idemployee=2,DateStart=DateTime.Parse("2019-01-01"),DateEnd=DateTime.Parse("2019-12-31")},
                    new Contract(){Idstudent=4,Idemployee=4,DateStart=DateTime.Parse("2019-01-01"),DateEnd=DateTime.Parse("2019-12-31")},
                    new Contract(){Idstudent=5,Idemployee=2,DateStart=DateTime.Parse("2019-01-01"),DateEnd=DateTime.Parse("2019-12-31")},

                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };


            if (_context.Functions.Count() == 0)
            {
                _context.Functions.AddRange(new List<Function>()
                {

                      new Function() {KeyId = "UserItem", Name = "User Management",ParentId = null,SortOrder = 1,Status = Status.Active,URL = "/",IconCss ="fa-chevron-down"  },

                      new Function() {KeyId = "View", Name = "View",ParentId = "UserItem",SortOrder = 1,Status = Status.Active,URL = "/Users",IconCss = "fa-home"  },
                      new Function() {KeyId = "Authorize", Name = "Authorization",ParentId = "UserItem",SortOrder = 2,Status = Status.Active,URL = "/Authorize",IconCss = "fa-home"  },

                      new Function() {KeyId = "Student", Name = "Student Management",ParentId = null,SortOrder = 2,Status = Status.Active,URL = "/",IconCss =  "fa-box"  },

                      new Function(){KeyId="StudentManage",Name="Manage", ParentId="Student",SortOrder=1 , Status= Status.Active, URL="/Students",IconCss="fa-home"},
                      new Function(){KeyId="StudentImport",Name="Import/Export List", ParentId="Student",SortOrder=2 , Status= Status.Active, URL="/ImportStudents",IconCss="fa-home"},

                      new Function() {KeyId = "Room", Name = "Building/Room",ParentId = null,SortOrder = 3,Status = Status.Active,URL = "/",IconCss =  "fa-chevron-down"  },

                      new Function() {KeyId = "Diagram", Name = "Diagram",ParentId = "Room",SortOrder = 1,Status = Status.Active,URL = "/Diagrams",IconCss =  "fa-home"  },
                      new Function() {KeyId = "RoomManage", Name = "Room",ParentId = "Room",SortOrder = 2,Status = Status.Active,URL = "/Rooms",IconCss =  "fa-home"  },
                      new Function() {KeyId = "Category", Name = "Category Room",ParentId = "Room",SortOrder = 3,Status = Status.Active,URL = "/CategoryRooms",IconCss =  "fa-home"  },

                      new Function() {KeyId = "Invoice", Name = "Invoice",ParentId = null,SortOrder = 4,Status = Status.Active,URL = "/",IconCss =  "fa-chevron-down"  },

                      new Function() {KeyId = "Manage", Name = "Manage Invoices",ParentId = "Invoice",SortOrder = 1,Status = Status.Active,URL = "/Invoices",IconCss =  "fa-home"  },
                      new Function() {KeyId = "Import", Name = "Import Invoices",ParentId = "Invoice",SortOrder = 2,Status = Status.Active,URL = "/Import",IconCss =  "fa-box"  },
                      new Function() {KeyId = "Export", Name = "Export Invoices",ParentId = "Invoice",SortOrder = 3,Status = Status.Active,URL = "/Export",IconCss =  "fa-box"  },
                       
                      new Function(){KeyId="Follow",Name ="Follow", ParentId=null, SortOrder =5 ,Status = Status.Active, URL="/",IconCss =  "fa-chevron-down" },




                 });
            }

            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };






        }
    }
}
