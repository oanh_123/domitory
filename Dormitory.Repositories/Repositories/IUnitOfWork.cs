﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dormitory.Repositories.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        bool Commit();
    }
}
