﻿using Dormitory.Entity.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dormitory.Repositories.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DormitoryContext _context;

        public UnitOfWork(DormitoryContext context)
        {
            _context = context;
        }

        public bool Commit()
        {
            try
            {
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
               
                return false;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
