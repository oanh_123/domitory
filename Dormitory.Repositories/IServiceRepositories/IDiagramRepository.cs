﻿using Dormitory.Entity.Data;
using Dormitory.Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Dormitory.Repositories.IServiceRepositories
{
    public interface IDiagramRepository :IRepository<Room,int>
    {
    }
}
