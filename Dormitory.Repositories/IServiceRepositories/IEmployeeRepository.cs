﻿using Dormitory.Entity.Data;
using Dormitory.Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Repositories.IServiceRepositories
{
    public interface IEmployeeRepository : IRepository<Employee,int>
    {

    }
}
