﻿using Dormitory.Entity.Data;
using Dormitory.Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Repositories.ServiceRepositories
{
     public interface IFunctionRepository : IRepository<Function,string>
    {
    }
}
