﻿using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Repositories.ServiceRepositories
{
    public class EmployeeRepository : Repository<Employee, int>, IEmployeeRepository
    {
        DormitoryContext _context;
        public EmployeeRepository(DormitoryContext context) : base(context)
        {
            _context = context;
        }
    }
}
