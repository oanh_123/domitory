﻿using Dormitory.Entity.Data;
using Dormitory.Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Repositories.ServiceRepositories
{
    public class FunctionRepository : Repository<Function,string>, IFunctionRepository
    {
        public FunctionRepository(DormitoryContext contextEntities) : base(contextEntities)
        {
        }
    }
}
