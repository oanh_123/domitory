﻿using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Repositories.ServiceRepositories
{
    public class RuleRepository : Repository<Rules, int>, IRuleRepository
    {
        private readonly DormitoryContext _context;
        public RuleRepository(DormitoryContext context) : base(context)
        {
            _context = context;
        }
    }
}
