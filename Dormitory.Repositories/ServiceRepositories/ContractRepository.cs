﻿using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Repositories.ServiceRepositories
{
    public class ContractRepository : Repository<Contract, int>, IContractRepository
    {
        private DormitoryContext _context;
        public ContractRepository(DormitoryContext context) : base(context)
        {
            _context = context;
        }
    }
}
