﻿using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Repositories.ServiceRepositories
{
    public class FollowRepository : Repository<Follow, int>, IFollowRepository
    {
        private readonly DormitoryContext _context;
        public FollowRepository(DormitoryContext context) : base(context)
        {
            _context = context;
        }
    }
}
