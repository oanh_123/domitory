﻿using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Repositories.ServiceRepositories
{
    public class InvoiceRepository : Repository<Invoice, int>, IInvoiceRepository
    {
        DormitoryContext _context;
        public InvoiceRepository(DormitoryContext context) : base(context)
        {
            _context = context;
        }
    }
}
