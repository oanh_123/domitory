﻿using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Repositories.ServiceRepositories
{
    public class DetailRepository : Repository<DetailInvoice, int>, IDetailRepository
    {
        DormitoryContext _context;
        public DetailRepository(DormitoryContext context) : base(context)
        {
            _context = context;
        }
    }
}
