﻿using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Repositories.ServiceRepositories
{
    public class CategoryRoomRepository : Repository<CategoryRoom, int>, ICategoryRoomRepository

    {
        DormitoryContext _context;

        public CategoryRoomRepository(DormitoryContext context) : base(context)
        {
            _context = context;
        }

    }

}
