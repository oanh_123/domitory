﻿
using Dormitory.Entity.Data;
using Dormitory.Ultilities.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.ViewModel
{
    public class RoomViewModel
    {
        public int KeyId { get; set; }
        public int Idcategory { get; set; }
        public int Idemployee { get; set; }      
        public int Idzone { get; set; }
        public Condition Status { get; set; }
        public string Name { get; set; }
        public int Reservation { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public virtual CategoryRoomViewModel IdcategoryNavigation { get; set; }
        public virtual EmployeeViewModel IdemployeeNavigation { get; set; }
        public virtual ZoneViewModel IdzoneNavigation { get; set; }
        public ICollection<StudentViewModel> Student { get; set; }


    }
}
