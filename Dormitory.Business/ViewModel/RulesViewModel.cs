﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.ViewModel
{
    public class RulesViewModel
    {
        public int KeyId { get; set; }
        public string Description { get; set; }
        public string Punish { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}
