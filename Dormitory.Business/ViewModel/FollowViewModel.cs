﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.ViewModel
{
    public class FollowViewModel
    {
        public int KeyId { get; set; }
        public int Idstudent { get; set; }
        public int Iderror { get; set; }
        public int Count { get; set; }
        public DateTime Date { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }

        public RulesViewModel IderrorNavigation { get; set; }
        public StudentViewModel IdstudentNavigation { get; set; }
       
    }
}
