﻿using Dormitory.Entity.Data;
using Dormitory.Ultilities.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Dormitory.Business.ViewModel
{
    public class StudentViewModel
    {
        public int KeyId { get; set; }
        [DisplayName("ID Student")]
        [Required(ErrorMessage = "ID Student is required")]
        public string IdStudent { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Gender is required")]
        public Gender Gender { get; set; }
        [DisplayName("Date of birth")]
        [Required(ErrorMessage = "Date of birth is required")]
        public DateTime DOB { get; set; }
        public string Faculty { get; set; }
        public string Class { get; set; }
        [DisplayName("Phone Number")]
        [Required(ErrorMessage = "Phone number is required")]
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        [DisplayName("Phone Number of Parents")]
        [Required(ErrorMessage = "Phone number of parents is required")]
        public string PhoneParents { get; set; }
        public string PolicyArea { get; set; }
        [DisplayName("Email of Teacher")]
        [Required(ErrorMessage = "Email of teacher is required")]
        public string Teacher { get; set; }
        [Required]
        public int Idroom { get; set; }
        [Required(ErrorMessage = "Email  is required")]
        public string Email { get; set; }
        [DisplayName("Identity Card")]
        [Required(ErrorMessage = "Identity Card is required")]
        public string Idcard { get; set; }
        public string Image { get; set; }
        public string AddressOfParents { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }

    }
}
