﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.ViewModel
{
    public class DetailInvoiceViewModel
    {
        public int KeyId { get; set; }
        public int InvoiceFK { get; set; }
        public int ServiceFK { get; set; }
        public int Number { get; set; }
        public decimal Money { get; set; }

        public virtual ServiceViewModel ServiceFKNavigation { get; set; }

    }
}