﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.ViewModel
{
    public class ContractViewModel
    {
        public int KeyId { get; set; }
        public int Idstudent { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public int Idemployee { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }

        public virtual EmployeeViewModel IdemployeeNavigation { get; set; }
        public virtual StudentViewModel IdstudentNavigation { get; set; }

    }
}
