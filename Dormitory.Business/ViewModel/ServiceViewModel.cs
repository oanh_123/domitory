﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.ViewModel
{
    public class ServiceViewModel
    {
        public int KeyId { get; set; }
        public string NameService { get; set; }
        public decimal UnitPrice { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}
