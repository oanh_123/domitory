﻿using Dormitory.Entity.Data;
using Dormitory.Ultilities.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.ViewModel
{
    public class InvoiceViewModel
    {
        public int KeyId { get; set; }
        public int Idroom { get; set; }
        public int Idemployee { get; set; }
        public DateTime Date { get; set; }
        public decimal TotalMoney { get; set; }
        public Status Status { get; set; }
        public string Name { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }

        public EmployeeViewModel IdemployeeNavigation { get; set; }
        public RoomViewModel IdroomNavigation { get; set; }
        public List<DetailInvoiceViewModel> DetailInvoice { get; set; }

    }
}
