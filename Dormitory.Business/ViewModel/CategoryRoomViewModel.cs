﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.ViewModel
{
    public class CategoryRoomViewModel
    {
        public int KeyId { get; set; }        
        public decimal UnitPrice { set; get; }
        public string Description { set; get; }
        public int Amount { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }

    }
}
