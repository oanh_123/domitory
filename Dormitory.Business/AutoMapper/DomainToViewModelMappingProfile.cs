﻿using AutoMapper;
using Dormitory.Business.ViewModel;
using Dormitory.Entity.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Zone, ZoneViewModel>();
            CreateMap<CategoryRoom, CategoryRoomViewModel>();
            CreateMap<Room, RoomViewModel>().ReverseMap();
            CreateMap<Student, StudentViewModel>();
            CreateMap<Invoice, InvoiceViewModel>().ReverseMap();              
            CreateMap<Dormitory.Entity.Data.Service, ServiceViewModel>().ReverseMap();
            CreateMap<DetailInvoice, DetailInvoiceViewModel>().ReverseMap();
            CreateMap<Employee, EmployeeViewModel>().ReverseMap();
            CreateMap<Contract, ContractViewModel>().ReverseMap();
            CreateMap<Follow, FollowViewModel>();
            CreateMap<Rules, RulesViewModel>();
            
        }

    }
}
