﻿using AutoMapper;
using Dormitory.Business.ViewModel;
using Dormitory.Entity.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.AutoMapper
{
    public class ViewModelToDomainMappingProfile :Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<RoomViewModel, Room>().ConstructUsing(c => new Room(c.KeyId, c.Idcategory,c.Idemployee,c.Idzone,c.Status,c.Name,c.Reservation,c.DateCreated,c.DateModified));
            CreateMap<ZoneViewModel, Zone>().ConstructUsing(c => new Zone(c.KeyId, c.Name,c.DateCreated,c.DateModified));
            CreateMap<CategoryRoomViewModel, CategoryRoom>().ConstructUsing(c => new CategoryRoom(c.KeyId, c.UnitPrice, c.Description, c.Amount, c.DateCreated, c.DateModified));
            CreateMap<EmployeeViewModel, Employee>().ConstructUsing(c => new Employee(c.KeyId, c.Name, c.PhoneNumber, c.Email, c.Position, c.DateCreated, c.DateModified));
            CreateMap<StudentViewModel, Student>().ConstructUsing(c => new Student(c.KeyId, c.IdStudent, c.Name, c.Gender, c.DOB, c.Faculty, c.Class, c.PhoneNumber, c.Address, c.PhoneParents, c.PolicyArea, c.Teacher, c.Idroom, c.Email, c.Idcard, c.Image, c.AddressOfParents, c.DateCreated, c.DateModified));
            CreateMap<ServiceViewModel, Dormitory.Entity.Data.Service>().ConstructUsing(c => new Entity.Data.Service(c.KeyId, c.NameService, c.UnitPrice, c.DateCreated, c.DateModified));
            CreateMap<InvoiceViewModel, Invoice>().ConstructUsing(c => new Invoice(c.KeyId, c.Idroom, c.Idemployee, c.Date, c.TotalMoney, c.Status, c.Name, c.DateCreated, c.DateModified));
            CreateMap<DetailInvoiceViewModel, DetailInvoice>().ConstructUsing(c => new DetailInvoice(c.KeyId, c.InvoiceFK, c.ServiceFK, c.Number, c.Money));
            CreateMap<ContractViewModel, Contract>().ConstructUsing(c => new Contract(c.KeyId, c.Idstudent, c.DateStart, c.DateEnd, c.Idemployee, c.DateCreated, c.DateModified));
            CreateMap<FollowViewModel, Follow>().ConstructUsing(c => new Follow(c.KeyId, c.Idstudent, c.Iderror, c.Count, c.Date, c.DateCreated, c.DateModified));
            CreateMap<RulesViewModel, Rules>().ConstructUsing(c => new Rules(c.KeyId, c.Description, c.Punish, c.DateModified, c.DateCreated));
        }
    }
}
