﻿using AutoMapper;
using Dormitory.Business.Interface;
using Dormitory.Business.ViewModel;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.Service
{
    public class ServiceService : IService
    {
        IServiceRepository _serviceRepository;
        IUnitOfWork _unitOfWork;
        public ServiceService(IServiceRepository serviceRepository, IUnitOfWork unitOfWork)
        {
            _serviceRepository = serviceRepository;
            _unitOfWork = unitOfWork;
        }

        public ServiceViewModel Add(ServiceViewModel deltailVm)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<ServiceViewModel> GetAll()
        {
            var query = _serviceRepository.FindAll();
            var data = new List<ServiceViewModel>();
            foreach (var item in query)
            {
                var _data = Mapper.Map<Dormitory.Entity.Data.Service, ServiceViewModel>(item);
                data.Add(_data);
            }
            return data;
        }

        public PageResult<ServiceViewModel> GetAllPaging(int? status, string keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public ServiceViewModel GetById(int id)
        {
            return Mapper.Map<Dormitory.Entity.Data.Service, ServiceViewModel>(_serviceRepository.FindById(id));
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public void Update(ServiceViewModel deltailVm)
        {
            throw new NotImplementedException();
        }
    }
}
