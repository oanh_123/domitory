﻿using AutoMapper.QueryableExtensions;
using Dormitory.Business.Interface;
using Dormitory.Entity.Data;
using Dormitory.Repositories.ServiceRepositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dormitory.Business.Service
{
    public class FunctionService : IFunction
    {
        IFunctionRepository _functionRepository;
        public FunctionService(IFunctionRepository functionRepository)
        {
            _functionRepository = functionRepository;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public Task<List<Function>> GetAll()
        {
            return _functionRepository.FindAll().ToListAsync();
        }

       

        public List<Function> GetByPermission(Guid userId)
        {
            throw new NotImplementedException();
        }

    }
}
