﻿using AutoMapper;
using Dormitory.Business.Interface;
using Dormitory.Business.ViewModel;
using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.Service
{
    public class CategoryRoomService : ICategoryRoom
    {
        private ICategoryRoomRepository _categoryRoomRepository;
        private IUnitOfWork _unitOfWork;

        public CategoryRoomService(ICategoryRoomRepository categoryRoomRepository, IUnitOfWork unitOfWork)
        {
            _categoryRoomRepository = categoryRoomRepository;
            _unitOfWork = unitOfWork;
        }

        public CategoryRoomViewModel Add(CategoryRoomViewModel roomVm)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<CategoryRoomViewModel> GetAll()
        {
            var query = _categoryRoomRepository.FindAll();
            var data = new List<CategoryRoomViewModel>();
            foreach (var item in query)
            {
                var _data = Mapper.Map<CategoryRoom, CategoryRoomViewModel>(item);
                data.Add(_data);
            }
            return data;
        }

        public PageResult<CategoryRoomViewModel> GetAllPaging(int? categoryID, string keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public CategoryRoomViewModel GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public void Update(CategoryRoomViewModel roomVm)
        {
            throw new NotImplementedException();
        }
    }
}
