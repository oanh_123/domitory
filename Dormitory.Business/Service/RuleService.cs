﻿using Dormitory.Business.Interface;
using Dormitory.Business.ViewModel;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.Service
{
    public class RuleService : IRule
    {
        private readonly IRuleRepository _ruleRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RuleService(IRuleRepository ruleRepository, IUnitOfWork unitOfWork)
        {
            _ruleRepository = ruleRepository;
            _unitOfWork = unitOfWork;
        }

        public void Add(RulesViewModel ruleVm)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<RulesViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public PageResult<RulesViewModel> GetAllPaging(int? status, string keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public RulesViewModel GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public void Update(RulesViewModel ruleVm)
        {
            throw new NotImplementedException();
        }
    }
}
