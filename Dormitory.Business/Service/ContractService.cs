﻿using AutoMapper;
using Dormitory.Business.Interface;
using Dormitory.Business.ViewModel;
using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dormitory.Business.Service
{
    public class ContractService : IContract
    {
        private readonly IContractRepository _contractRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ContractService(IContractRepository contractRepository, IUnitOfWork unitOfWork)
        {
            _contractRepository = contractRepository;
            _unitOfWork = unitOfWork;
        }

        public void Add(ContractViewModel contractVm)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<ContractViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public PageResult<ContractViewModel> GetAllPaging(int? status, string keyword, int page, int pageSize)
        {
            var query = _contractRepository.FindAll();
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Idstudent.Equals(keyword));
            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize).Take(pageSize);
            var data = new List<ContractViewModel>();
            foreach (var item in query)
            {
                var _data = Mapper.Map<Contract, ContractViewModel>(item);
                data.Add(_data);
            }
            var paginationSet = new PageResult<ContractViewModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize,
            };
            return paginationSet;
        }

        public ContractViewModel GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public void Update(ContractViewModel contractVm)
        {
            throw new NotImplementedException();
        }
    }
}
