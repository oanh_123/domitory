﻿using AutoMapper;
using Dormitory.Business.Interface;
using Dormitory.Business.ViewModel;
using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dormitory.Business.Service
{
    public class FollowService : IFollow
    {
        private readonly IFollowRepository _followRepository;
        private readonly IUnitOfWork _unitOfWork;

        public FollowService(IFollowRepository followRepository, IUnitOfWork unitOfWork)
        {
            _followRepository = followRepository;
            _unitOfWork = unitOfWork;
        }

        public FollowViewModel Add(FollowViewModel followVm)
        {
            var follow = Mapper.Map<FollowViewModel, Follow>(followVm);
            _followRepository.Add(follow);
            _unitOfWork.Commit();
            return followVm;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<FollowViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public PageResult<FollowViewModel> GetAllPaging(string keyword, int page, int pageSize)
        {
            var query = _followRepository.FindAll(x=> x.IdstudentNavigation, x=> x.IderrorNavigation);
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.IdstudentNavigation.IdStudent.Equals(keyword) 
                || x.IdstudentNavigation.Name.Contains(keyword));
            //if (categoryID.HasValue)
            //    query = query.Where(x => x.Idcategory == categoryID.Value);
            //if (status.HasValue)
            //{
            //    if (status == 2)
            //    {
            //        query = query.Where(x => x.Status == Condition.StillEmpty && x.Reservation > 0);

            //    }
            //    else
            //    {
            //        query = query.Where(x => x.Status == (Condition)status);
            //    }
            //}

            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize).Take(pageSize);
            var data = new List<FollowViewModel>();
            foreach (var item in query)
            {
                
                var _data = Mapper.Map<Follow, FollowViewModel>(item);
                data.Add(_data);
            }
            var paginationSet = new PageResult<FollowViewModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize,
            };
            return paginationSet;
        }

        public FollowViewModel GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public void Update(FollowViewModel followVm)
        {
            throw new NotImplementedException();
        }
    }
}
