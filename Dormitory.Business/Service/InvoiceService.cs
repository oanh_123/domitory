﻿using AutoMapper;
using Dormitory.Business.Interface;
using Dormitory.Business.ViewModel;
using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dormitory.Business.Service
{
    public class InvoiceService : IInvoice
    {
        private IInvoiceRepository _invoiceRepository;
        private IServiceRepository _serviceRepository;
        private IDetailRepository _detailRepository;
        private IUnitOfWork _unitOfWork;
        public InvoiceService(IInvoiceRepository invoiceRepository, IUnitOfWork unitOfWork,
            IServiceRepository serviceRepository, IDetailRepository detailRepository)
        {
            _invoiceRepository = invoiceRepository;
            _unitOfWork = unitOfWork;
            this._serviceRepository = serviceRepository;
            _detailRepository = detailRepository;
        }

        public void Add(InvoiceViewModel invoiceVm)
        {
            invoiceVm.Date = DateTime.Now;

            try
            {
                var invoice = Mapper.Map<InvoiceViewModel, Invoice>(invoiceVm);
                var detailInvoice = Mapper.Map<List<DetailInvoiceViewModel>, List<DetailInvoice>>(invoiceVm.DetailInvoice); 
                foreach (var item in invoice.DetailInvoice)
                {
                    var query = _serviceRepository.FindById(item.ServiceFK);
                    item.Money = item.Number * query.UnitPrice;
                    invoice.TotalMoney += item.Money;
                }
                //invoiceVm.DetailInvoice = detailInvoice;
                _invoiceRepository.Add(invoice);
                _unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<InvoiceViewModel> GetAll()
        {
            var query = _invoiceRepository.FindAll(x => x.IdroomNavigation);
            var data = new List<InvoiceViewModel>();
            foreach (var item in query)
            {
                var _data = Mapper.Map<Invoice, InvoiceViewModel>(item);
                data.Add(_data);
            }
            return data;
        }

        public PageResult<InvoiceViewModel> GetAllPaging( int? status, string keyword, int page, int pageSize)
        {
            var query = _invoiceRepository.FindAll(x=>x.IdroomNavigation,x=>x.IdemployeeNavigation);
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Name.Contains(keyword));
            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize).Take(pageSize);
            var data = new List<InvoiceViewModel>();
            foreach (var item in query)
            {             
                var _data = Mapper.Map<Invoice,InvoiceViewModel>(item);
                data.Add(_data);
            }
            var paginationSet = new PageResult<InvoiceViewModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize,
            };
            return paginationSet;

        }

        public InvoiceViewModel GetById(int id)
        {
            var query = _invoiceRepository.FindById(id,x => x.DetailInvoice, x=>x.IdroomNavigation,x=> x.IdemployeeNavigation);
            foreach(var item in query.DetailInvoice)
            {
                item.ServiceFKNavigation = _serviceRepository.FindById(item.ServiceFK);
            }
            return Mapper.Map<Invoice, InvoiceViewModel>(query);

            //return Mapper.Map<Invoice, InvoiceViewModel>(_invoiceRepository.FindById(id,x=> x.DetailInvoice));
        }

        public bool Save()
        {
            return _unitOfWork.Commit();
        }
        public void Update(InvoiceViewModel invoiceVm)
        {
            var objectChange = _invoiceRepository.FindById(invoiceVm.KeyId,x=>x.DetailInvoice);
            if (objectChange != null)
            {
                objectChange.Name = invoiceVm.Name;
                objectChange.Idroom = invoiceVm.Idroom;
                objectChange.Idemployee = invoiceVm.Idemployee;
                var detailInvoice = Mapper.Map<List<DetailInvoiceViewModel>, List<DetailInvoice>>(invoiceVm.DetailInvoice);
                foreach (var item in invoiceVm.DetailInvoice)
                {                  
                        //var query = _serviceRepository.FindById(item.ServiceFK);
                        var detailChange = _detailRepository.FindById(item.KeyId);
                    _detailRepository.Update(detailChange);
                    _unitOfWork.Commit();
                   
                }

            }
        }
        public int GetLastest()
        {
            var invoice = _invoiceRepository.FindAll();
            invoice = invoice.OrderByDescending(x => x.KeyId);
            var _invoice = invoice.First();
            return _invoice.KeyId;
        }
    }

       
    
}
