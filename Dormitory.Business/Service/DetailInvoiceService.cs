﻿using AutoMapper;
using Dormitory.Business.Interface;
using Dormitory.Business.ViewModel;
using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.Service
{
    public class DetailInvoiceService : IDetailInvoice
    {
        private IDetailRepository _detailRepository;
        private IUnitOfWork _unitOfWork;
        public DetailInvoiceService(IDetailRepository detailRepository, IUnitOfWork unitOfWork)
        {
            _detailRepository = detailRepository;
            _unitOfWork = unitOfWork;
        }

        public DetailInvoiceViewModel Add(DetailInvoiceViewModel detailVm)
        {
            var detailInvoice = Mapper.Map<DetailInvoiceViewModel, DetailInvoice>(detailVm);
            _detailRepository.Add(detailInvoice);
            _unitOfWork.Commit();
            return detailVm;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<DetailInvoiceViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public PageResult<DetailInvoiceViewModel> GetAllPaging(int? status, string keyword, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public DetailInvoiceViewModel GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public void Update(DetailInvoiceViewModel detailVm)
        {
            var objectChange = _detailRepository.FindById(detailVm.KeyId);
            if (objectChange != null)
            {
                objectChange.ServiceFK = detailVm.ServiceFK;
                objectChange.Number = detailVm.Number;
              
            }
        }
    }
}
