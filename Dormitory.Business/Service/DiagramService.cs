﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Dormitory.Business.Interface;
using Dormitory.Business.ViewModel;
using Dormitory.Entity.Data;
using Dormitory.Repositories.IServiceRepositories;
using Dormitory.Repositories.Repositories;
using Dormitory.Ultilities.DTOs;
using Dormitory.Ultilities.Enum;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Dormitory.Business.Service
{
    public class DiagramService : IDiagram
    {
        private IDiagramRepository _diagramRepository;
        private IUnitOfWork _unitOfWork;
        private ICategoryRoomRepository _categoryRoomRepository;

        public DiagramService(IDiagramRepository diagramRepository , IUnitOfWork unitOfWork, ICategoryRoomRepository categoryRoomRepository
            )
        {
            _diagramRepository = diagramRepository;
            _unitOfWork = unitOfWork;
            this._categoryRoomRepository = categoryRoomRepository;
        }

        public RoomViewModel Add(RoomViewModel roomVm)
        {
            var category = _categoryRoomRepository.FindById(roomVm.Idcategory);
            roomVm.Reservation = category.Amount;     
            var room = Mapper.Map<RoomViewModel, Room>(roomVm);           
            _diagramRepository.Add(room);
            _unitOfWork.Commit();
            return roomVm;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<RoomViewModel> GetAll()
        {

            var query = _diagramRepository.FindAll(x => x.IdcategoryNavigation);
            var data = new List<RoomViewModel>();
            foreach (var item in query)
            {
                var _data = Mapper.Map<Room, RoomViewModel>(item);
                data.Add(_data);
            }
            return data;

        }

        public PageResult<RoomViewModel> GetAllPaging(int? categoryID,int? status,string keyword, int page, int pageSize)
        {
            var query = _diagramRepository.FindAll(x=>x.IdcategoryNavigation,x=>x.Student);
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Name.Contains(keyword));
            if (categoryID.HasValue)
                query = query.Where(x => x.Idcategory == categoryID.Value);
            if(status.HasValue)
            {
                if (status == 2)
                {
                    query = query.Where(x => x.Status == Condition.StillEmpty && x.Reservation > 0);
                 
                }
                else
                {
                    query = query.Where(x => x.Status == (Condition)status);
                }              
            }
           
            int totalRow = query.Count();
            query = query.Skip((page - 1) * pageSize).Take(pageSize);
            var data = new List<RoomViewModel>();
            foreach (var item in query)
            {               
                if (item.IdcategoryNavigation.Amount == item.Student.Count())
                {
                    item.Status = Condition.Full;
                    _unitOfWork.Commit();
                }               
                var _data = Mapper.Map<Room, RoomViewModel>(item);
                data.Add(_data);
            }
            var paginationSet = new PageResult<RoomViewModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize,
            };
            return paginationSet;
         
        }

        public RoomViewModel GetById(int id)
        {
            return Mapper.Map<Room, RoomViewModel>(_diagramRepository.FindById(id,x=> x.IdcategoryNavigation, x=> x.Student));
        }

        public Task<List<RoomViewModel>> GetByIdZone()
        {
            throw new NotImplementedException();
        }

        public RoomViewModel Reversation(int id)
        {
            try
            {
                var query = _diagramRepository.FindById(id);
                if (query.Reservation > 0)
                {
                    query.Reservation = query.Reservation - 1;
                    _unitOfWork.Commit();

                }
                var data = new RoomViewModel();
                data = Mapper.Map<Room, RoomViewModel>(query);

                return data;
            }
           catch(Exception ex)
            {
                throw ex;
            }
           
        }

        public bool Save()
        {
            return _unitOfWork.Commit();
        }

        public void Update(RoomViewModel roomVm)
        {
            var objectChange = _diagramRepository.FindById(roomVm.KeyId);
            if (objectChange != null)
            {
                objectChange.Name = roomVm.Name;
                objectChange.Idzone = roomVm.Idzone;
                objectChange.Idcategory = roomVm.Idcategory;
                objectChange.Idemployee = roomVm.Idemployee;
                objectChange.Status = roomVm.Status;
                objectChange.Reservation = roomVm.Reservation;
            }
        }
    }
}
