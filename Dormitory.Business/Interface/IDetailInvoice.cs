﻿using Dormitory.Business.ViewModel;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dormitory.Business.Interface
{
    public interface IDetailInvoice :IDisposable
    {
        DetailInvoiceViewModel Add(DetailInvoiceViewModel detailVm);
        void Update(DetailInvoiceViewModel detailVm);
        List<DetailInvoiceViewModel> GetAll();
        PageResult<DetailInvoiceViewModel> GetAllPaging( int? status, string keyword, int page, int pageSize);
        DetailInvoiceViewModel GetById(int id);
        bool Save();

    }
}
