﻿using Dormitory.Business.ViewModel;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.Interface
{
    public interface IFollow : IDisposable
    {
        FollowViewModel Add(FollowViewModel followVm);
        void Update(FollowViewModel followVm);
        List<FollowViewModel> GetAll();
        PageResult<FollowViewModel> GetAllPaging(string keyword,int page, int pageSize);
        FollowViewModel GetById(int id);
        bool Save();
    }
}
