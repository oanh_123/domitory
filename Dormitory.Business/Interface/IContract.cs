﻿using Dormitory.Business.ViewModel;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.Interface
{
    public interface IContract : IDisposable
    {

        void Add(ContractViewModel contractVm);
        void Update(ContractViewModel contractVm);
        List<ContractViewModel> GetAll();
        PageResult<ContractViewModel> GetAllPaging(int? status, string keyword, int page, int pageSize);
        ContractViewModel GetById(int id);
        bool Save();
    }
}
