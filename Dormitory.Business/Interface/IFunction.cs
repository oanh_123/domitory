﻿using Dormitory.Entity.Data;
using Dormitory.Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dormitory.Business.Interface
{
    public interface IFunction : IDisposable
    {
        Task<List<Function>> GetAll();
        List<Function> GetByPermission(Guid userId);
    }
}
