﻿using Dormitory.Business.ViewModel;
using Dormitory.Entity.Data;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dormitory.Business.Interface
{
    public interface IDiagram :IDisposable
    {
        RoomViewModel Add(RoomViewModel roomVm);
        void Update(RoomViewModel roomVm);
        List<RoomViewModel> GetAll();
        PageResult<RoomViewModel> GetAllPaging(int? categoryID,int? status,string keyword, int page, int pageSize);
        Task<List<RoomViewModel>> GetByIdZone();
        RoomViewModel GetById(int id);
        RoomViewModel Reversation(int id);
        bool Save();
    }
}
