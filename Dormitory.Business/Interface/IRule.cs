﻿using Dormitory.Business.ViewModel;
using Dormitory.Repositories.Repositories;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.Interface
{
   public interface IRule : IDisposable
    {
        void Add(RulesViewModel ruleVm);
        void Update(RulesViewModel ruleVm);
        List<RulesViewModel> GetAll();
        PageResult<RulesViewModel> GetAllPaging(int? status, string keyword, int page, int pageSize);
        RulesViewModel GetById(int id);
        bool Save();
    }
}
