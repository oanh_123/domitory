﻿using Dormitory.Business.ViewModel;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.Interface
{
    public interface ICategoryRoom : IDisposable
    {
        CategoryRoomViewModel Add(CategoryRoomViewModel roomVm);
        void Update(CategoryRoomViewModel roomVm);
        List<CategoryRoomViewModel> GetAll();
        PageResult<CategoryRoomViewModel> GetAllPaging(int? categoryID, string keyword, int page, int pageSize);
        CategoryRoomViewModel GetById(int id);
        bool Save();
    }
}
