﻿using Dormitory.Business.ViewModel;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.Interface
{
    public interface IService :IDisposable
    {
        ServiceViewModel Add(ServiceViewModel deltailVm);
        void Update(ServiceViewModel deltailVm);
        List<ServiceViewModel> GetAll();
        PageResult<ServiceViewModel> GetAllPaging(int? status, string keyword, int page, int pageSize);
        ServiceViewModel GetById(int id);
        bool Save();
    }
}
