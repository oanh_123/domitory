﻿using Dormitory.Business.ViewModel;
using Dormitory.Ultilities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.Interface
{
    public interface IInvoice : IDisposable
    {
        void Add( InvoiceViewModel invoiceVm);
        void Update( InvoiceViewModel invoiceVm);
        List< InvoiceViewModel> GetAll();
        PageResult< InvoiceViewModel> GetAllPaging( int? status, string keyword, int page, int pageSize);
        InvoiceViewModel GetById(int id);
        bool Save();
        int GetLastest();
    }
}
