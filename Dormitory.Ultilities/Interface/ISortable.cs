﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Ultilities.Interface
{
    public interface ISortable
    {
        int SortOrder { set; get; }
    }
}
