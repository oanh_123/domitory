﻿using Dormitory.Ultilities.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.Interface
{
    public interface ISwitchable
    {
        Status Status { set; get; }
    }
}
