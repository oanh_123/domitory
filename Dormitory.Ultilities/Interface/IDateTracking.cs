﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dormitory.Business.Interface
{
    public interface IDateTracking
    { 
     DateTime? DateCreated { get; set; }
    DateTime? DateModified { get; set; }
    }
}
